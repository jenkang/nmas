<?php
/* 
Plugin Name: NeverMissASign 3.0.0
Version: 3.0.0
Plugin URI: http://nevermissasign.com
Description: Plugin for creating wristbands [wb2_generate] [wb2_manage]
Author: NeverMissASign enhanced by Epicenter
Author URI: http://nevermissasign.com
License: GPL v3
 */

require_once( dirname( __FILE__ ) . '/admin/SettingsPage.php' );

if ( ! class_exists( 'Wristbander2' ) ) {
	class Wristbander2 {		
		var $objects_table;
		var $id;
		var $open;
		var $delete;
		var $current_user;
		var $manage_url;
		var $generate_url;
		var $nonce_name = 'wristbander2_nonce';
		var $nonce_action = 'wb2_nonceit';
		var $options;

		var $color_array = array(
		    "black" => "#000000",
		    "red" => "#ff0000",
		    "orange" => "#ffa500",
		    "yellow" => "#ffff00",
		    "green" => "#008000",
		    "blue" => "#0000ff",
		    "violet" => "#cc00cc",
		    "twitter blue" => "#33ccff",
		    "burnt orange" => "#cc6600",
		    "brown" => "#663300",
		    "gold" => "#ffd700",
		    "navy blue" => "#000080",
		    "purple" => "#461d7c",
		    "crimson" => "#a32638",
		    "maroon" => "#660000",	
		    "grey" => "#808080",
		    "silver" => "#c0c0c0",
		    "carolina blue" => "#7bafd4",
		    "pink" => "#ffc0cb",
		    "fuchsia" => "#ff00ff",	
		    "forrest Green" => "#003300",
		    "lime green" => "#00ff00",
		    "auburn orange" => "#dd550c"
		);

		var $black_font_color_array = array(
			"yellow" => "#ffff00",
			"gold" => "#ffd700",
			"silver" => "#c0c0c0",
			"pink" => "#ffc0cb",
			"lime green" => "#00ff00"
			);

		var $highlight_color_array = array(
		    "yellow" => "#ffff66",
		    "blue" => "#aaffff",
		    "orange" => "#ffa500",
		    "lime green" => "#00ff00",
		    "purple" => "#d15fee",
		    "pink" => "#ffc0cb"
		);

		var $grid_array = array(
			"3x3 (54 boxes)" => "3",
			"4x4 (96 boxes)" => "4",
			"5x5 (150 boxes)" => "5",
			"6x6 (216 boxes)" => "6"
			);

		var $grid_boxes_array = array(
			"3" => "54",
			"4" => "96",
			"5" => "150",
			"6" => "216"
			);

		public function __construct() {
            global $wpdb;
                        
			// Properties
            $this->objects_table = $wpdb->prefix . 'saved';
			$this->id = (int) @$_GET['wb2_id'];
			$this->open = @$_GET['wb2_open'];
			$this->edit = @$_GET['wb2_edit'];
			$this->delete = @$_GET['wb2_delete'];
			
			// Activation
			register_activation_hook( __FILE__, array( $this, 'wb2_activate' ) );
			
			// Init
			add_action( 'init', array( $this, 'wb2_init' ) );
			
			// Register js and css
			add_action( 'wp_enqueue_scripts', array( $this, 'wb2_register' ) );
			
			// Register shortcodes
			add_shortcode( 'wb2_generate', array( $this, 'wb2_generate' ) );
			add_shortcode( 'wb2_manage', array( $this, 'wb2_manage' ) );
			
			// Actions
			add_action( 'template_redirect', array( $this, 'wb2_process' ) );
		}
		public function wb2_activate() {
			$this->db_tables();
		}
		public function wb2_init() {
			global $current_user;
			
			$this->current_user = $current_user;
			$this->options = get_option( 'wristbander2_options' );
			$this->manage_url = get_permalink( $this->options['manage_page'] );
			$this->generate_url = get_permalink( $this->options['generate_page'] );
		}
		public function wb2_register() {
			// Register js scripts
			wp_register_script( 'jquery.wristbander2', plugins_url( '/js/jquery.wristbander2.js', __FILE__ ), array( 'jquery', 'jquery-ui-core', 'jquery-ui-slider', 'jquery-touch-punch' ), 1.0 );
			
			// Register styles
			wp_register_style( 'jquery.ui.css', '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css' );
			wp_register_style( 'wristbander2', plugins_url( 'css/style.css', __FILE__ ) );
			
			wp_enqueue_style( 'jquery.ui.css' );
			wp_enqueue_script( 'jquery.wristbander2' );
			wp_enqueue_style( 'wristbander2' );
		}
		public function wb2_generate() {
			if ( ! is_user_logged_in() ) {
				return;
			}			
			
			return $this->wb2_generate_html();
		}
		public function wb2_manage() {	
			if ( ! is_user_logged_in() ) {
				return;
			}
			
			if ( $this->id ) {
				if ( $this->open ) {
					$error_message = "";
					if( isset( $_POST['wb2_x'] ) && isset( $_POST['wb2_y'] ) ){
						$error_message = $this->wb2_validate_width_height( $_POST['wb2_x'], $_POST['wb2_y'] );
					}

					return $this->wb2_manage_open($error_message);
				}
			} else {
				return $this->wb2_manage_list();
			}			
		}
		/***********************************************************************
		 * Internal Helpers
		 **********************************************************************/
		private function db_tables() {
			global $wpdb;

			$structures=array(
				"CREATE TABLE IF NOT EXISTS `{$this->objects_table}` (
					`ID` int(10) AUTO_INCREMENT PRIMARY KEY,
					`name` varchar(255) NOT NULL,
					`date` datetime NOT NULL,
					`grid` tinyint(3) NOT NULL DEFAULT 5,
					`data5` text NOT NULL,
					`data4` text NOT NULL,
					`data3` text NOT NULL,
					`data2` text NOT NULL,
					`data` text NOT NULL,
					`user` int(10) NOT NULL,
					`active` int(1) NOT NULL DEFAULT 1,
					`color` varchar(32) NULL DEFAULT NULL,
					`sets` int(11) NOT NULL,
					`fba` text NULL)"
			);
				
			foreach($structures AS $structure){
				$wpdb->query($structure);
			}
		}
		public function wb2_process() {
			if ( ! isset( $_POST['wb2_action'] ) ) {
				$this->wb2_process_manage();
				
				return;
			}
			
			$action = $_POST['wb2_action'];
			
			if ( $action == 'add_new' ) {
				$this->wb2_process_generate();
			}
		}
		private function wb2_process_generate() {
			if ( !wp_verify_nonce($_POST[$this->nonce_name], $this->nonce_action) ) {
				wp_die("Cheatin' uh?");
			}
			
			global $wpdb, $current_user;
				
			$sets = $_POST['wb2_sets'];
			$theDelimiter = '|-|';
			
			if($sets>10) $sets=10;

			$total = count($_POST['wb2_names']);		

			if($this->id) {
				// $sets=1;
			}

			for($l=0; $l < $sets; $l++) {

				$times = array();
				$times1 = array();	
				$times2 = array();
				$times3 = array();
				
				switch ($_POST['wb2_grid']) {
				    case 3:
				        $shared = 54;
				        break;
				    case 4:
				        $shared = 96;
				        break;
				    case 5:
				        $shared = 150;
				        break;
				     case 6:
				        $shared = 216;
				        break;
				}	

				$total_count=0;

				for($i=0; $i<count($_POST['wb2_fba']); $i++) {	
					$val = $_POST['wb2_fba'][$i];
					$total_count+=$val;
				}

				for($i=0; $i<count($_POST['wb2_fba']); $i++) {	
					$val = $_POST['wb2_fba'][$i];
					$percent = $val/$total_count;
					$num = floor($shared * $percent);
					for($x=0; $x<$num; $x++) {
						$times1[] = $_POST['wb2_names'][$i].$theDelimiter.$_POST['wb2_highlight_colors'][$i];
					}
				}

				$max = count($_POST['wb2_names']) - 1;

				while(count($times1)<$shared) {
					$i = rand(0, $max);
					$times1[] = $_POST['wb2_names'][$i].$theDelimiter.$_POST['wb2_highlight_colors'][$i];
					// add random players to fill
				}		

				shuffle($times1);

				for($i=0; $i<count($times1); $i++) {
					$code_parts = explode($theDelimiter, $times1[$i]);
					$times2[] = $code_parts[0];
					$times3[] = $code_parts[1];
				}

				$counter=0;
				$data = json_encode($times2);

				if($l>0) {
					$name .= "  #" . ($l+1); 
				}

				$fba   = json_encode($_POST['wb2_fba']);
				$data5 = json_encode($times3);
				$data4 = json_encode($_POST['wb2_highlight_colors']);
				$data3 = json_encode($_POST['wb2_names']);
				$data2 = json_encode($_POST['wb2_names2']);

				if($this->id && $_POST['wb2_sets'] == 1) {
					$wpdb->update( 
						$this->objects_table, 
						array( 
							'fba'		=>	$fba,
							'name'		=>	$_POST['wb2_name'] . ' ' . $name,
							'date'		=>	current_time('mysql', 1),
							'grid'		=>	$_POST['wb2_grid'],
							'data5'		=>	$data5,
							'data4'		=>	$data4,
							'data3'		=>	$data3,
							'data2'		=>	$data2,
							'data'		=>	$data,
							'user'		=>	$current_user->ID,
							'active'	=>	1,
							'color'		=>	$_POST['wb2_color'],
							'sets'		=>	$_POST['wb2_sets']
						), 
						array( 'ID' => $this->id ), 
						array( 
							'%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s', '%s', '%s',
						), 
						array( '%d' ) 
					);
					
				} else {
					$wpdb->insert( 
						$this->objects_table, 
						array( 
							'fba'		=>	$fba,
							'name'		=>	$_POST['wb2_name'] . ' ' . $name,
							'date'		=>	current_time('mysql', 1),
							'grid'		=>	$_POST['wb2_grid'],
							'data5'		=>	$data5,
							'data4'		=>	$data4,
							'data3'		=>	$data3,
							'data2'		=>	$data2,
							'data'		=>	$data,
							'user'		=>	$current_user->ID,
							'active'	=>	1,
							'color'		=>	$_POST['wb2_color'],
							'sets'		=>	$_POST['wb2_sets']
						), 
						array( 
							'%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s', '%s', '%s',
						) 
					);
				}
				
				unset($name);
			}

			wp_redirect($this->manage_url);
			exit();
		}
		private function wb2_generate_html() {
			global $wpdb;
			
			if($this->id) { 
				$row = $wpdb->get_row("SELECT * FROM {$this->objects_table} WHERE user='" . $this->current_user->ID . "' AND ID='" . $this->id . "'", ARRAY_A);
				
				if ( ! $row ) {
					return '<p class="error">Sorry, this wristband doesn\'t exist.</p>';
				}
			} 
			
			if(!$row['fba']) $row['fba']=100;
			if(!$row['fbi']) $row['fbi']=100;
			if(!$row['ch']) $row['cb']=100;
			if(!$row['sl']) $row['sl']=100;
			if(!$row['ch']) $row['ch']=100;
			if(!$row['p']) $row['p']=100;
			
			$fba = array();
			$grid = 5;

			if($this->id) {
				$names  = json_decode($row['data3']);
				$names2 = json_decode($row['data2']);
				$highlight_colors = json_decode($row['data4']);
				$fba = json_decode($row['fba']);
				$grid = $row['grid'];

			} else { 
				$data = array();
				$names = array("FBA");
				$names2 = array("Fastball Away");
			}

			$row['fba']=100;
			$total=0;

			for($i=0; $i<count($names); $i++) {
				$value = $fba[$i];

				if(!$this->id || !$fba[$i]) {
					$value = 100;
				} 

				$total += $value;
			}
			
			ob_start(); ?>

			<div id="wb2_generate">
				<form action="" method="POST">
					<input type="hidden" name="wb2_action" value="add_new" />
					<?php wp_nonce_field($this->nonce_action, $this->nonce_name ); ?>
					<h4>Name</h4>
					<div class="form-field">
						<input type="text" name="wb2_name" id="wristband_name" placeholder="Enter the name of this wristband" value="<?php echo $row['name']; ?>" />
						<p class="wb2_description">Enter the desired name of this set of wristbands. You will use this later to find your wristbands in your database.</p>
					</div>
					<h4>Color</h4>
					<div class="form-field">
						<div class="wb-select-color-wrapper">
							<div class="wb-selected-color" style="background-color: <?php if( isset($row['color']) ){ echo $row['color']; }else{ echo '#000000';} ?>"></div>
							<select class="wb-select-color" name="wb2_color" style="width:200px;">
								<?php 
									foreach ( $this->color_array as $key => $value ) {
									    echo '<option value="'.$value.'"';
									     if( $row['color'] == $value ){
									     	echo ' selected';
									     }  
									     echo '>'.ucwords($key).'</option>';
									}
								?>
							</select>
						</div>
						<p class="wb2_description">Select the desired color for this set of wristbands.</p>
					</div>
					<h4>Grid Size</h4>
					<div class="form-field">
						<select name="wb2_grid" style="width:200px;">
							<?php 
								foreach ( $this->grid_array as $key => $value ) {
								    echo '<option data-boxes="'.$this->grid_boxes_array[$value].'" value="'.$value.'"';
								     if( $grid == $value ){
								     	echo ' selected';
								     }  
								     echo '>'.ucwords($key).'</option>';
								}
							?>
						</select>
						<p class="wb2_description">Select the desired grid size for this set of wristbands.</p>
					</div>

					<h4>Number of sets</h4>
					<div class="form-field">
						<select name="wb2_sets">
						<?php for($i=1; $i<11; $i++) : ?>
							<?php if( $this->id ) { ?>
								<option value="<?php echo $i; ?>"<?php if($i==1) echo " selected"; ?>><?php echo $i; ?> Set<?php if($i>1) echo "s"; ?> </option>
							<?php } else { ?>
								<option value="<?php echo $i; ?>"<?php if($row['sets']==$i) echo " selected"; ?>><?php echo $i; ?> Set<?php if($i>1) echo "s"; ?> </option>
							<?php }; ?>
						<?php endfor; ?>
						</select>
						<p class="wb2_description">Each set shuffles the signals differently using the same ratio of signs.  The same # of each sign will be on different sets, but you can switch them out throughout the season.</p>
						<p class="wb2_description wb2_urgent">Note: If you have 12 players you do NOT need 12 different sets.  You will need to make 1 set and print 12 copies.</p>
					</div>

					<h4>Signs</h4>
					<div class="form-field">
					<?php for($i=0; $i<count($names); $i++) : ?>
					<?php
						if(!$this->id || !$fba[$i]) {
							$fba = array();
							$fba[$i] = 100;
						} 

						$percent = sprintf("%0.2f", $fba[$i]/$total*100);
						$color="grey";

						if($percent<5) {
							$color = "red";
						}
					?>
						<div class="data">
							<div class="data-left">
								<table id="wb2_generate_sets">
									<tr>
										<td><input type="text" name="wb2_names[]" value="<?php echo $names[$i]; ?>" maxlength="5" style="width:100px;"/></td>
										<td><input type="text" name="wb2_names2[]" value="<?php echo $names2[$i]; ?>" maxlength="50" /></td>
										<td>
											<select name="wb2_highlight_colors[]" style="width:175px;height: 30px!important;display:inline;">
												<option value="#ffffff">No Highlight</option>
												<?php 
													foreach ( $this->highlight_color_array as $key => $value ) {
													    echo '<option value="'.$value.'"';
													     if($highlight_colors[$i] == $value ){
													     	echo ' selected';
													     }  
													     echo '>Highlight '.ucwords($key).'</option>';
													}
												?>
											</select>
										</td>
										<td>
											<span class="mobile-only">Add/Remove Signs: </span>
											<a id="wb2_add" href="#"><img src="<?php echo plugins_url('/images/add.png', __FILE__); ?>" alt="" /></a>
											<a id="wb2_remove" href="#" class="remove" style="<?php if(count($names)<=1) echo "display:none"; ?>"><img src="<?php echo plugins_url('/images/delete.png', __FILE__); ?>" alt="" /></a>
										</td>
									</tr>
								</table>
							</div>
							<div class="data-number">(<span class="<?php echo $color; ?>"><?php echo $percent; ?>%</span> of Signs)</div>	
							<div class="clear"></div>
							<div id="fba" class="slider"></div>
							<input type="hidden" name="wb2_fba[]" value="<?php echo $fba[$i]; ?>" /> 
						</div>
						<?php endfor; ?>
						<p class="wb2_description desktop-only">In the box to the left, enter an acronym for each sign (maximum length for legibility is 5). In the box to the right, enter the full name of the sign. Move the slider left or right to decrease and increase its frequency on the card. Finally, use the "+" and "-" toggles to add or remove signs.</p>
						<p class="wb2_description mobile-only">In the top box, enter an acronym for each sign. In the bottom box, enter the full name of the sign. Move the slider left or right to decrease and increase its frequency on the card. Finally, use the "+" and "-" toggles to add or remove signs.</p>
					</div>
					<input type="submit" name="submit" class="button button-primary" />
				</form>
			</div>
		<?php 
			$result = ob_get_contents(); ob_end_clean();
			
			return $result;
		}
		private function wb2_process_manage() {
			if ( $this->delete ) {
				$this->wb2_manage_delete();
			} elseif ( $_POST['wb2_view'] ) {

				$error_message = "";

				if( isset( $_POST['wb2_x'] ) && isset( $_POST['wb2_y'] ) ){
					$error_message = $this->wb2_validate_width_height( $_POST['wb2_x'], $_POST['wb2_y'] );

					if( strlen($error_message) ){
						$this->wb2_manage_open($error_message);
					} else {
						$this->wb2_manage_download();
					}
				}

			} elseif( $_POST['wb2_view_coach_wristband'] ) {
				$this->wb2_manage_download_coach_wristband();
			} elseif( $_POST['wb2_view_coach'] ) {
				$this->wb2_manage_download_coach();
			}
		}
		private function wb2_validate_width_height($w, $h){
			$width_floor = 2.75;
			$width_ceiling = 6;
			$height_floor = 1.75;
			$height_ceiling = 4;
			$error_message = "";

			if( !is_numeric( $w ) ){
				$error_message = "Width is not a number.";
			}

			if( !is_numeric( $h ) ){
				if( strlen($error_message) ){
					$error_message = $error_message . "<br>";
				}
				$error_message = $error_message . "Height is not a number.";
			}

			if( $w < $width_floor ){
				if( strlen($error_message) ){
					$error_message = $error_message . "<br>";
				}
				$error_message = $error_message . "Width is too small to print legibly.";
			}

			if( $w > $width_ceiling ){
				if( strlen($error_message) ){
					$error_message = $error_message . "<br>";
				}
				$error_message = $error_message . "Width is too large. Width limit is " + width_ceiling + " inches.";
			}

			if( $h < $height_floor ){
				if( strlen($error_message) ){
					$error_message = $error_message . "<br>";
				}
				$error_message = $error_message . "Height is too small to print legibly.";
			}

			if( $h > $height_ceiling ){
				if( strlen($error_message) ){
					$error_message = $error_message . "<br>";
				}
				$error_message = $error_message . "Height is too large. Height limit is " + height_ceiling + " inches.";
			}

			if( $w < $h ){
				if( strlen($error_message) ){
					$error_message = $error_message . "<br>";
				}
				$error_message = $error_message . "Width must be greater than height to print legibly.";
			}
			return $error_message;
		}

		private function wb2_manage_list() {
			global $wpdb;
			
			$query = $wpdb->get_results("SELECT * FROM {$this->objects_table} WHERE user='" . $this->current_user->ID . "' and active='1' ORDER BY ID ASC", ARRAY_A); 
			
			if ( empty( $query ) ) {
				return sprintf( '<p class="error">No wristbands yet. <a href="%s">Click here to create one.</a></p>', $this->generate_url );
			}
			ob_start(); ?>
			<div id="wb2_manage_list">
			<h4>Your Generated Wristbands</h4>
				<table id="wb2_table_list">
					<tr>
						<th>Name</th>
						<th>Date</th>
						<th>Actions</th>
					</tr>
					<?php $count = 0; ?>
					<?php foreach ($query as $row) : ?>
						<?php ( $count % 2 == 0) ? $class = 'class="even"' : $class = 'class="odd"'; ?>
						<tr <?php echo $class; ?>>
							<td style="vertical-align:middle;"> <?php echo $row['name']; ?> </td>
							<td style="vertical-align:middle;"><?php  echo strftime("%m/%d/%Y", strtotime($row['date'])); ?> </td>
							<td style="width:350px; text-align: center;">
								<a class="button button-primary" href="<?php echo wp_nonce_url($this->manage_url . '?wb2_id=' . $row['ID'] . '&wb2_delete=1', $this->nonce_action, $this->nonce_name); ?>">Delete</a>
								<a class="button button-primary" href="<?php echo $this->generate_url; ?>?wb2_id=<?php echo $row['ID']; ?>">Edit</a>
								<a class="button button-primary" href="<?php echo $this->manage_url; ?>?wb2_id=<?php echo $row['ID']; ?>&wb2_open=1">Open</a>
							</td>
						</tr>
						<?php $count++; ?>
					<?php endforeach; ?>
				</table>
			</div>
		<?php 
			$result = ob_get_contents(); ob_end_clean();
			
			return $result;
		}
		private function wb2_manage_open($error_message = "") {
			global $wpdb;
			
			$row =  $wpdb->get_row("SELECT * FROM {$this->objects_table} WHERE user='" . $this->current_user->ID . "' AND ID='" . $this->id . "'", ARRAY_A);

			if( !$row ) {
				return '<div class="error">Sorry, that wristband doesn\'t exist.</div>';
			} 
			
			ob_start(); ?>
				<div id="wb2_manage_open">
				<h4>Download/Print Wristband: <?php echo $row['name']; ?></h4>
					<form class="wb-download-form" action="" method="POST">
						<?php wp_nonce_field( $this->nonce_action, $this->nonce_name ); ?>
						<div class="form-field">
							<table id="wb2_table_open">
								<tr>
									<td>Height (in inches)</td>
									<td>Width (in inches)</td>
								</tr>
								<tr>
									<td><input class="wb-input-height" type="text" name="wb2_y" value="<?php if( isset( $_POST['wb2_y'] ) ) { echo $_POST['wb2_y']; } else { echo "3";} ?>" maxlength="4"  /></td>
									<td><input class="wb-input-width" type="text" name="wb2_x" value="<?php if( isset( $_POST['wb2_x'] ) ) { echo $_POST['wb2_x']; } else { echo "5";} ?>" maxlength="4" /></td>
								</tr>
								<tr>
									<td colspan="2">
										Number of player wristbands this size will print on one page: <strong><span class="number-per-page">3</span></strong></br>
										<input type="submit" class="button button-primary wb-submit-view" name="wb2_view" value="Download Player Wristband" />
										<input type="submit" class="button button-primary" name="wb2_view_coach_wristband" value="Download Coach Wristband" />
										<input type="submit" class="button button-primary" name="wb2_view_coach" value="Download Coach" />
										<span class="wb-alert"><?php echo $error_message; ?></span>
									</td>
								</tr>
							</table>
						</div>
					</form>
				</div>
		<?php
			$result = ob_get_contents(); ob_end_clean();
			
			return $result;
		}
		private function wb2_manage_delete() {
			if ( !wp_verify_nonce( $_GET[$this->nonce_name], $this->nonce_action ) ) {
				wp_die("Cheatin' uh?");
			}
			
			global $wpdb;
			
			$wpdb->update( 
				$this->objects_table, 
				array( 
					'active' => 0
				), 
				array( 
					'ID' => $this->id, 
					'user' => $this->current_user->ID 
				), 
				array( 
					'%d'
				), 
				array( '%d', '%d' ) 
			);
							
			wp_redirect($this->manage_url);
			exit();
		}


		private function wb2_manage_download() {
			if ( ! wp_verify_nonce( $_POST[$this->nonce_name], $this->nonce_action ) ) {
				wp_die("Cheatin' uh?");
			}
			
			global $wpdb;

			$row = $wpdb->get_row("SELECT * FROM {$this->objects_table} WHERE ID='" . $this->id . "' AND user='" . $this->current_user->ID . "'", ARRAY_A);
			
			if ( ! $row ) {
				wp_die("There was a problem creating the PDF for this wristband.");
			}

			require_once( plugin_dir_path( __FILE__ ) . "extlib/dompdf-0.6.2/dompdf_config.inc.php");

			$old_limit = ini_set("memory_limit", "512M");
			$times2 = json_decode($row['data']);
			$sign_codes = json_decode($row['data3']);
			$highlight_data4 = false;
			if( strlen(trim($row['data5'])) === 0 ){
				$sign_codes_highlight = json_decode($row['data4']);
				$highlight_data4 = true;
			} else {
				$sign_codes_highlight = json_decode($row['data5']);
			}
					
			$name = $row['name'];
			$file_name = 'NMAS_' . str_replace(' ', '_', trim($name)) . '_player_'.time().'.pdf';
			$number_per_page = 2;

			if( $_POST['wb2_x']>8.5 ) 
				$_POST['wb2_x']=8.5; // 8.4	
				
			$x = (float) $_POST['wb2_x'];
			$y = (float) $_POST['wb2_y'];
			$row_color = sanitize_text_field($row['color']);
			$row_increment = 1;

			switch ($row['grid']) {
			    case 3:
			        $numbers1 = array("", "01", "02", "03", "", "11", "12", "13", "", "21", "22", "23");
			        $numbers2 = array("", "31", "32", "33", "", "41", "42", "43", "", "51", "52", "53");
			        break;
			    case 4:
			        $numbers1 = array("", "01", "02", "03", "04", "", "11", "12", "13", "14", "", "21", "22", "23", "24");
			        $numbers2 = array("", "31", "32", "33", "34", "", "41", "42", "43", "44", "", "51", "52", "53", "54");
			        break;
			    case 5:
			        $numbers1 = array("", "01", "02", "03", "04", "05", "", "11", "12", "13", "14", "15", "", "21", "22", "23", "24", "25");
			        $numbers2 = array("", "31", "32", "33", "34", "35", "", "41", "42", "43", "44", "45", "", "51", "52", "53", "54", "55");
			        break;
			     case 6:
			        $numbers1 = array("", "00", "01", "02", "03", "04", "05", "", "10", "11", "12", "13", "14", "15", "", "20", "21", "22", "23", "24", "25");
			        $numbers2 = array("", "30", "31", "32", "33", "34", "35", "", "40", "41", "42", "43", "44", "45", "", "50", "51", "52", "53", "54", "55");
			        $row_increment = 0;
			        break;
			}

			if( $y >= 1.75 && $y <= 2.25 ){
				$number_per_page = 4;
			}

			if( $y > 2.25 && $y <= 3.4 ){
				$number_per_page = 3;
			}

			$accepted = array();
			$black_font = array();
			$font_color = "#ffffff";

			foreach ( $this->color_array as $key => $value ) {
			   array_push ( $accepted, $value );
			}
			foreach ( $this->black_font_color_array as $key => $value ) {
			   array_push ( $black_font, $value );
			}

			// Checks
			if ( !in_array( $row_color, $accepted, true ) ) {
				$row_color = '#000000';
			}

			if ( in_array( $row_color, $black_font, true ) ) {
				$font_color = "#000000";
			}

			// Helpers
			$border = 2;
			$dpi = 72;
			$rows = $row['grid']*2+3;
			$cols = count($numbers1);
			$font_minimum = 5.5;
			$font_maximum = 7.5;

			// Variables
			$body_height = $y*$dpi;
			$body_weight = $x*$dpi;
			$height = ($y*$dpi)+5;
			$width = ($x*$dpi)+20;
			$td_height = round(($height/$rows),2)-0.75;
			$td_width = round(($width/$cols),2);

			$calc_font_size = ceil($td_width/2);
			$font_size = ceil($td_width/2)*0.75;

			$code_array = array_map('strlen', $sign_codes);
			$maxlen = max($code_array);
			$minlen = min($code_array);
			$avglen = array_sum($code_array)/count($code_array);
			$totallen = $maxlen + $minlen;

			if( $maxlen === 5 ){
				$calc_font_size = ceil($td_width/3);
				$font_size = ceil($td_width/3)*0.75;
				if ( $x <= 3 ){
					$font_size = ceil($td_width/2)*0.55;
				}
			}
			if( $maxlen === 4 ){
				$calc_font_size = ceil($td_width/2);
				$font_size = ceil($td_width/2)*0.75;
				if ( $x <= 3 ){
					$font_size = ceil($td_width/2)*0.65;
				}
			}

			if( $font_size < $font_minimum ){
				$font_size = $font_minimum;
			} 
			if ( $font_size > $font_maximum ){
				$font_size = $font_maximum;
			}
			
			$branding_font_size = $font_size;
			
			ob_start();
			?>
			<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
			<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
				<head>
					<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
					<title></title>
					<?php require_once(dirname(__FILE__) . '/templates/template-player.php'); ?>
				</head>
				<body>
					<?php for ($j = 1; $j <= $number_per_page; $j++)  :  $counter=0; ?>

						<table id="table" cellpadding="0" cellspacing="0">
							<tr>
							<?php foreach($numbers1 as $val) : ?>			
								<th><?php echo $val; ?></th>
							<?php endforeach; ?>
							</tr>
							<?php for($c=0; $c<$row['grid']; $c++) : ?>
								<tr>
								<?php for($i=0; $i<3; $i++) : ?> 
									<?php for($a=0; $a<$row['grid']+1; $a++) : ?>
										<?php if( $a==0 ) : ?>			
											<th><?php echo ($c+$row_increment); ?></th>
										<?php else : ?>
											<?php
												$highlight_color = "#ffffff";
												$highlight_index = array_search( trim($times2[$counter]), $sign_codes );
												if( $highlight_index !== false && $highlight_data4 ){
													$highlight_color = $sign_codes_highlight[$highlight_index];
												} else {
													$highlight_color = $sign_codes_highlight[$counter];
												}
											?>
											<td style="background-color:<?php echo $highlight_color; ?>"><?php 
											$this_code = ( strlen( trim($times2[$counter]) ) ) ? trim($times2[$counter]) : trim($times2[0]);
											echo '<span style="font-size:'.$font_size.'pt">'.preg_replace('/\s+/', '&nbsp;', $this_code).'</span>'; $counter++; 
											?></td>
										<?php endif; ?>
									<?php endfor; ?>
								<?php endfor; ?>
								</tr>
							<?php endfor; ?>
							<tr>
								<th colspan="<?php echo $cols ?>" class="narrow" style="font-size:<?php echo $branding_font_size; ?>"><?php echo trim($name); ?> - NeverMissASign.com</th>
							</tr>
							<tr>
							<?php foreach($numbers2 as $val) : ?>			
								<th><?php echo $val; ?></th>
							<?php endforeach; ?>
							</tr>
							<?php for($c=0; $c<$row['grid']; $c++) : ?>
								<tr>
								<?php for($i=0; $i<3; $i++) : ?>
									<?php for($a=0; $a<$row['grid']+1; $a++) : ?>
										<?php if($a==0) : ?>			
											<th><?php echo ($c+$row_increment); ?></th>
										<?php else : ?>
											<?php
												$highlight_color = "#ffffff";
												$highlight_index = array_search( trim($times2[$counter]), $sign_codes );
												if( $highlight_index !== false && $highlight_data4 ){
													$highlight_color = $sign_codes_highlight[$highlight_index];
												} else {
													$highlight_color = $sign_codes_highlight[$counter];
												}
											?>
											<td style="background-color:<?php echo $highlight_color; ?>"><?php 
											$this_code = ( strlen( trim($times2[$counter]) ) ) ? trim($times2[$counter]) : trim($times2[0]);
											echo '<span style="font-size:'.$font_size.'pt">'.preg_replace('/\s+/', '&nbsp;', $this_code).'</span>'; $counter++; 
											?></td>
										<?php endif; ?>
									<?php endfor; ?>
								<?php endfor; ?>

								</tr>
							<?php endfor; ?>	
						</table>

					<?php endfor; ?>
					<p style="text-align:center; font-size:4px; padding-bottom:5px;"><?php  echo 'f: '.$font_size.' s: '.$y.'x'.$x; ?></p>
				</body>
			</html>
			<?php
			$html = ob_get_contents();
			ob_end_clean();
			
			$dompdf = new DOMPDF();
			$dompdf->load_html($html);
			$dompdf->render();
			$dompdf->stream($file_name);
			exit();	
			/*
			<table id="legend">
				<tr>
					<td style="text-align:center; font-size:12px; padding-bottom:5px;">
						<?php
						$names2 = json_decode($row['data2'], TRUE);
						$names = json_decode($row['data3'], TRUE);

						for($i=0; $i<count($names); $i++) {
							printf('<p>%s=%s</p>', $names[$i], $names2[$i]);
							$array[] = $names[$i];
						} ?>
					</td>
				</tr>
			</table>
			*/
			// return;
		}

		private function wb2_manage_download_coach_wristband() {
			if ( ! wp_verify_nonce( $_POST[$this->nonce_name], $this->nonce_action ) ) {
				wp_die("Cheatin' uh?");
			}
			
			global $wpdb;
			
			$row = $wpdb->get_row("SELECT * FROM {$this->objects_table} WHERE ID='" . $this->id . "' AND user='" . $this->current_user->ID . "'", ARRAY_A);
			$times2 = json_decode($row['data']);
			$counter=0;
			$row_color = sanitize_text_field($row['color']);
			$file_name = 'NMAS_' . str_replace(' ', '_', trim($row['name'])) . '_coach_'.time().'.pdf';
			$grid = $row['grid'];
			$name = $row['name'];

			$sign_codes = json_decode($row['data3']);
			$sign_codes_highlight = json_decode($row['data4']);

			$maxcolumns_per_row = 9;
			$maxcolumns = 9;	

			$highlight_data4 = false;

			$row_color = sanitize_text_field($row['color']);

			$accepted = array();
			$black_font = array();
			$branding_font_color = "#ffffff";

			foreach ( $this->color_array as $key => $value ) {
			   array_push ( $accepted, $value );
			}
			foreach ( $this->black_font_color_array as $key => $value ) {
			   array_push ( $black_font, $value );
			}

			// Checks
			if ( !in_array( $row_color, $accepted, true ) ) {
				$row_color = '#000000';
			}

			if ( in_array( $row_color, $black_font, true ) ) {
				$branding_font_color = "#000000";
			}

			if( strlen(trim($row['data5'])) === 0 ){
				$highlight_grid = json_decode($row['data4']);
				$highlight_data4 = true;
			} else {
				$highlight_grid = json_decode($row['data5']);
			}

			for($y=0; $y<2; $y++) {
				if($y==1) $add=30;
				
				for($c=0; $c<$grid; $c++) { 
					$x = $c+1;
					for($i=0; $i<3; $i++) { 
						for($a=0; $a<$grid; $a++) { 
							if( $grid == 6 ){
								$number = ($i)*(10)+$a+$add;
								$number2 = $c;
							} else {
								$number = ($i)*(10)+$a+1+$add;
								$number2 = $c+1;
							}
							$number = sprintf("%02d", $number);	
							$highlight_index = array_search( trim($times2[$counter]), $sign_codes );
							if( $highlight_index !== false && $highlight_data4 ){
								$highlight_color = $highlight_grid[$highlight_index];
							} else {
								$highlight_color = $highlight_grid[$counter];
							}	
							$list[$number . $number2] = $times2[$counter] . $highlight_color;
							$counter++;
						} 
					}
				}
			}

			$names2 = json_decode($row['data2'], TRUE); 
			$names = json_decode($row['data3'], TRUE);
			$cellcount = count($names) + (3-count($names)%3);
			
			for($i=1; $i<=$cellcount; $i++) {

				if ( isset($names[$i-1]) ){
					$array[] = $names[$i-1];
				} 
			}

			// Helpers
			$dpi = 72;
			$cols = 9;
			$font_size = 7.5;
	
			$bigarray = $array;
			unset($array);
			$iteration = 0;
			// Variables
			
			$x = (float) $_POST['wb2_x'];
			$y = (float) $_POST['wb2_y'];

			$font_offset   = 0;
			$height_offset = 2;
			$width_offset  = 10;

			if ( $y > 2.5 ){
				$font_offset = 1;
			}

			if ( count($bigarray) > 9 && count($bigarray) <= 12 ) {
				$cols = 4;
				$font_size = 7;
			} else if ( count($bigarray) > 12 && count($bigarray) <= 15 ) {
				$cols = 5;
				$font_size = 7;
			} else if ( count($bigarray) > 15 && count($bigarray) <= 18 ) {
				$cols = 6;
				$font_size = 7;
			} else if ( count($bigarray) > 18 && count($bigarray) <= 21 ) {
				$cols = 7;
				$font_size = 7;
			} else if ( count($bigarray) > 21 && count($bigarray) <= 24 ) {
				$cols = 8;
				$font_size = 6;
			} else if ( count($bigarray) > 24 && count($bigarray) <= 27 ) {
				$cols = 9;
				$font_size = 6;
			} else if ( count($bigarray) > 28 ) {
				$cols = 10;
				$font_size = 6;
			}

			$font_size += $font_offset;
			$rows = ceil(count($bigarray)/$cols)*2;

			if($rows > 2){
				$height_offset = 2;
			}

			$height = ($y*$dpi)-10;
			$width = ($x*$dpi)+10;
			$height_css = $height . "px";
			$width_css = $width . "px";
			$th_height_css = (round(($height/$rows),2)-$height_offset)*0.2 . "px";
			$td_height_css = (round(($height/$rows),2)-$height_offset)*1.8 . "px";
			$td_width_css = round(($width/$cols),2)-$width_offset . "px";

			if (count($bigarray) <= $maxcolumns){
				$cols = count($bigarray);
			}

			require_once( dirname(__FILE__) . "/extlib/dompdf-0.6.2/dompdf_config.inc.php");
			$old_limit = ini_set("memory_limit", "512M");
			
			ob_start(); ?><!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
			<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
			<title></title>
				<?php require_once(dirname(__FILE__) . '/templates/template-coach-wristband.php'); ?>
			</head>
			<body>	
				<table>
					<tr>
						<th colspan="<?php echo $cols; ?>" style="border: 1px solid <?php echo $row_color; ?>;background-color: <?php echo $row_color; ?>; color: <?php echo $branding_font_color; ?>"><?php echo $name . " - NeverMissASign.com"; ?></th>
					</tr>

					<?php
						$count=1;
						$iteration = 0;
						$iterations = $rows/2;
						$code_count = 0;

					for($x=1; $x<=$iterations; $x++) {	
						$iteration++;
						$array = array();
						$colcount = $cols*$iteration;

						for($i=$count; $i<=$colcount; $i++) {
							$array[] = $bigarray[$i-1];	
							$count++;
						}
					?>		
				
						<thead>
						<tr>
							<?php
							
							foreach($array as $key=>$val) { 
								$this_index = $key;
								$empty_cells = 0;
								if($rows == $count){
									$empty_cells = $cols - (count($array) % $cols);
								}
								
								if ($iteration > 1 ){
									$this_index = $key + ($cols * ($iteration - 1));
								}
								$highlight_color = $sign_codes_highlight[$this_index];
								$text_color = "#000000";
								
								if( $highlight_color === "#000000"){
									$text_color = "#ffffff";
								}
								
								$style = "background-color:" . $highlight_color . ";color:" . $text_color. ";";
								?>
							<th style="<?php echo $style ?>"><?php echo trim($val) ?></th>
							<?php } 
							if ($empty_cells > 0 && $empty_cells < $cols){
								for($e=0; $e<$empty_cells; $e++) {
									 echo "<th>&nbsp;</th>";
								}
							}
							?>
						</tr>
						</thead>
						<tr>
							<?php
							
							foreach($array as $index=>$val) { 	
								$this_index = $index;
								$empty_cells = 0;
								if($rows == $count){
									$empty_cells = $cols - (count($array) % $cols);
								}
								
								if ($iteration > 1 ){
									$this_index = $index + $cols;
								}	
							?>
							<td>
								<?php
								$values = array();

								foreach($list as $key=>$val2) { 
									$highlight_color = $sign_codes_highlight[$this_index];
									if(strtolower($val2)==strtolower($val.$highlight_color)) {					
										$values[] = sprintf("%03d", $key);									
									}
								} 

								sort($values);
								$code_count += count($values);
								
								
								foreach($values as $index=>$val) {
									 echo $val;
									 if($index != count($values) - 1){
									 	echo ", ";
									 }
								}
								?>
							</td>
							
							<?php 
							} 
							if ($empty_cells > 0 && $empty_cells < $cols){
								for($e=0; $e<$empty_cells; $e++) {
									 echo "<td>&nbsp;</td>";
								}
							}
							?>
						</tr>
					
				<?php } ?>
				</table>
				<div style="text-align:center;margin-top: 20px;width: 99%"><small><?php echo $name . "<br>" . $_POST['wb2_y'] . " x " . $_POST['wb2_x'] . "<br>grid size: " . $grid ." x " . $grid . "<br>number of codes: " .  $code_count . "<br>font size: " .  $font_size; ?></small></div>	
				<div style="position: absolute;bottom: 10px; text-align:center;width: 99%">NeverMissASign.com</div>
			</body></html><?php
			$html = ob_get_contents();
			ob_end_clean();


			$dompdf = new DOMPDF();
			$dompdf->load_html($html);
			$html = null;
			$bigarray = null;
			$dompdf->render();
			$dompdf->stream($file_name);	
			exit();		
		}

		private function wb2_manage_download_coach() {
			if ( ! wp_verify_nonce( $_POST[$this->nonce_name], $this->nonce_action ) ) {
				wp_die("Cheatin' uh?");
			}
			
			global $wpdb;
			
			$row = $wpdb->get_row("SELECT * FROM {$this->objects_table} WHERE ID='" . $this->id . "' AND user='" . $this->current_user->ID . "'", ARRAY_A);
			$card_name = $row['name'];
			$times2 = json_decode($row['data']);
			$counter=0;
			$row_color = sanitize_text_field($row['color']);
			$file_name = 'NMAS_' . str_replace(' ', '_', trim($row['name'])) . '_coach_'.time().'.pdf';
			$grid = $row['grid'];

			$sign_codes = json_decode($row['data3']);
			$sign_codes_highlight = json_decode($row['data4']);

			$highlight_data4 = false;
			if( strlen(trim($row['data5'])) === 0 ){
				$highlight_grid = json_decode($row['data4']);
				$highlight_data4 = true;
			} else {
				$highlight_grid = json_decode($row['data5']);
			}

			for($y=0; $y<2; $y++) {
				if($y==1) $add=30;
				
				for($c=0; $c<$grid; $c++) { 
					$x = $c+1;
					for($i=0; $i<3; $i++) { 
						for($a=0; $a<$grid; $a++) { 
							if( $grid == 6 ){
								$number = ($i)*(10)+$a+$add;
								$number2 = $c;
							} else {
								$number = ($i)*(10)+$a+1+$add;
								$number2 = $c+1;
							}
							$number = sprintf("%02d", $number);	
							$highlight_index = array_search( trim($times2[$counter]), $sign_codes );
							if( $highlight_index !== false && $highlight_data4 ){
								$highlight_color = $highlight_grid[$highlight_index];
							} else {
								$highlight_color = $highlight_grid[$counter];
							}	
							$list[$number . $number2] = $times2[$counter] . $highlight_color;
							$counter++;
						} 
					}
				}
			}

			require_once( dirname(__FILE__) . "/extlib/dompdf-0.6.2/dompdf_config.inc.php");
			$old_limit = ini_set("memory_limit", "512M");
			
			ob_start(); ?><!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
			<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
			<title></title>
				<?php require_once(dirname(__FILE__) . '/templates/template-coach.php'); ?>
			</head>
			<body>	
				<table id="legend">
					<?php
						$names2 = json_decode($row['data2'], TRUE); 
						$names = json_decode($row['data3'], TRUE);
						$cellcount = count($names) + (3-count($names)%3);
						
						echo '<tr><td>&nbsp;</td>';
						echo '<td width="200" style="text-align:center;padding-bottom:10px;line-height: auto;"><img style="width: 200px; max-width: 200px;" src="'. dirname(__FILE__) . '/images/nmas_logo2.png" /></td>';
						echo '<td>&nbsp;</td></tr>';

						for($i=1; $i<=$cellcount; $i++) {
							$row = $i % 3;
							if ( $i == 1 ) {
								echo '<tr>';
							} 

							if ( isset($names[$i-1]) ){
								echo '<td>' . $names[$i-1] . " = " . $names2[$i-1] . '</td>';
								$array[] = $names[$i-1];
							} else {
								echo '<td>&nbsp;</td>';
							}

							if ( $i == $cellcount ){
								echo '</tr>';
							} elseif ( $row == 0 && $i<$cellcount) {
								echo '</tr><tr>';
							}
						}
					?>
				</table>
				<?php
				$bigarray = $array;
				unset($array);
				$iteration = 0;
				$maxcolumns_per_row = 10;
				$maxcolumns = 10;	
			
				$lasttable = false;

				for($x=1; $x<=count($bigarray); $x+=$maxcolumns) {	
					$iteration++;
					$array = array();
					$count=0;
					$colcount = $maxcolumns*$iteration;

					if( (count($bigarray) - $maxcolumns) <= 0 ){
						$maxcolumns = count($bigarray);
						$colcount = $maxcolumns*$iteration;

					} elseif( (count($bigarray) - $colcount) <= 0 ){
						$maxcolumns = count($bigarray) - $colcount + $maxcolumns;
						$colcount = $x+$maxcolumns-1;
						$lasttable = true;
					}

					for($i=$x; $i<=$colcount; $i++) {
						if($count==$colcount) {
							break;
						}
						$array[] = $bigarray[$i-1];	
						$count++;
					}
				?>		
					<table class="signs">
						<thead>
						<?php if ( $x == 1 ) { ?>
							<tr>
								<th colspan="<?php echo $maxcolumns; ?>" style="border: 1px solid #000;"><?php echo $card_name; ?></th>
							</tr>
						<?php } ?>
						<tr>
							<?php
							foreach($array as $key=>$val) { 
								$this_index = $key;
								if ($iteration > 1 ){
									$this_index = $key + ( $maxcolumns_per_row * ( $iteration - 1 ) );
								}
								$highlight_color = $sign_codes_highlight[$this_index];
								$text_color = "#000000";
								
								if( $highlight_color === "#000000"){
									$text_color = "#ffffff";
								}
								
								$style = "background-color:" . $highlight_color . ";color:" . $text_color. ";";
								?>
							<th style="border: 1px solid #000;white-space: nowrap;<?php echo $style ?>"><?php echo trim($val) ?></th>
							<?php } ?>
						</tr>
						</thead>
						<tr>
							<?php
							foreach($array as $index=>$val) { 	
								$this_index = $index;
								if ($iteration > 1 ){
									$this_index = $index + ( $maxcolumns_per_row * ( $iteration - 1 ) );
								}	
							?>
							<td style="border: 1px solid #000;">
								<?php
								$values = array();

								foreach($list as $key=>$val2) { 
									$highlight_color = $sign_codes_highlight[$this_index];
									if(strtolower($val2)==strtolower($val.$highlight_color)) {					
										$values[] = sprintf("%03d", $key);									
									}
								} 

								sort($values);
								
								foreach($values as $val) {
									 echo $val . " ";
								}
								?>
							</td>
							<?php } ?>
						</tr>
					</table>			
				<?php } ?>
				<div style="position: absolute;bottom: 10px; text-align:center;width: 99%">NeverMissASign.com</div>
			</body></html><?php
			$html = ob_get_contents();
			ob_end_clean();


			$dompdf = new DOMPDF();
			$dompdf->load_html($html);
			$html = null;
			$bigarray = null;
			$dompdf->render();
			$dompdf->stream($file_name);	
			exit();		
		}
	}
}

$wristbander2 = new Wristbander2;