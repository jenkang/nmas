<style>
@page { margin: 0px; }
html { margin: 0px; }
body {
	width: 100%;
	margin:0;
	padding:0;
	font-family: Helvetica;
	font-size: 100%;
}
table {
	width: <?php echo $width_css; ?>;
	height: <?php echo $height; ?>;
	margin: 10px auto 0 auto;
	border-spacing: 0;
	border-collapse: collapse;
	table-layout: fixed;
}


td, th {
	height: <?php echo $td_height_css; ?>;
	text-align: center;
	border: 1px solid #000;
	/*border-spacing: 0;
	border-collapse: collapse;*/
	font-size: <?php echo $font_size; ?>pt;
	vertical-align: middle;
}

 th {
 	font-size: <?php echo $font_size-1; ?>pt;
	height: <?php echo $th_height_css; ?>;
}

td {
	font-size: <?php echo $font_size; ?>pt;

}


</style>