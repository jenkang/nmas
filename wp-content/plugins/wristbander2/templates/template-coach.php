<?php
// Setup
$accepted = array();
$black_font = array();
$font_color = "#ffffff";

foreach ( $this->color_array as $key => $value ) {
   array_push ( $accepted, $value );
}
foreach ( $this->black_font_color_array as $key => $value ) {
   array_push ( $black_font, $value );
}

// Checks
if ( !in_array( $row_color, $accepted, true ) ) {
	$row_color = '#000000';
}

if ( in_array( $row_color, $black_font, true ) ) {
	$font_color = "#000000";
}
?>
<style>
body, html {
	/*font-family: Helvetica;*/
	font-size: 100%;
	padding: 0;
	margin: 0;
}

table {
	width: 96%;
	margin: 20px auto 0 auto;
	border: 1px solid #000;
	border-spacing: 0;
}

th, td {
	border: 1px solid #000;
	border-spacing: 0;
	text-align: center;
	text-transform: uppercase;
}

th {
	color:<?php echo $font_color; ?>;
	background-color: #f1f1f1;
	background-color: <?php echo $row_color; ?>;
}

td {
	line-height: 1.5em;
	padding:10px 0px 10px 2px;
	font-size:11px;
}

table.signs td{
	font-size:10px;
}

#legend {
	border: none;
}

#legend th,
#legend td {
	border: none;
	text-align: left;
	padding: 0;
}
</style>