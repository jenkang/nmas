<?php

namespace WLMStripe;

/**
 * Class Discount
 *
 * @property string $object
 * @property Coupon $coupon
 * @property string $customer
 * @property int $end
 * @property int $start
 * @property string $subscription
 *
 * @package WLMStripe
 */
class Discount extends WLMStripeObject
{

    const OBJECT_NAME = "discount";
}
