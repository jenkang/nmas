<?php

namespace WLMStripe;

/**
 * Class BitcoinTransaction
 *
 * @package WLMStripe
 */
class BitcoinTransaction extends ApiResource
{

    const OBJECT_NAME = "bitcoin_transaction";
}
