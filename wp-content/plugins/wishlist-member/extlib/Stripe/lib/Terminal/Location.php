<?php

namespace WLMStripe\Terminal;

/**
 * Class Location
 *
 * @property string $id
 * @property string $object
 * @property string $display_name
 * @property string $address_city
 * @property string $address_country
 * @property string $address_line1
 * @property string $address_line2
 * @property string $address_state
 * @property string $address_postal_code
 *
 * @package WLMStripe\Terminal
 */
class Location extends \WLMStripe\ApiResource
{
    const OBJECT_NAME = "terminal.location";

    use \WLMStripe\ApiOperations\All;
    use \WLMStripe\ApiOperations\Create;
    use \WLMStripe\ApiOperations\Retrieve;
    use \WLMStripe\ApiOperations\Update;
}
