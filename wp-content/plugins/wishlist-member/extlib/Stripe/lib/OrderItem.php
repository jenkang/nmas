<?php

namespace WLMStripe;

/**
 * Class OrderItem
 *
 * @property string $object
 * @property int $amount
 * @property string $currency
 * @property string $description
 * @property string $parent
 * @property int $quantity
 * @property string $type
 *
 * @package WLMStripe
 */
class OrderItem extends WLMStripeObject
{

    const OBJECT_NAME = "order_item";
}
