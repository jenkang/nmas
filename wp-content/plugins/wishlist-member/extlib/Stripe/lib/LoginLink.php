<?php

namespace WLMStripe;

/**
 * Class LoginLink
 *
 * @property string $object
 * @property int $created
 * @property string $url
 *
 * @package WLMStripe
 */
class LoginLink extends ApiResource
{

    const OBJECT_NAME = "login_link";
}
