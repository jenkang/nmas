<?php

namespace WLMStripe\Error;

class InvalidRequest extends Base
{
    public function __construct(
        $message,
        $WLMStripeParam,
        $httpStatus = null,
        $httpBody = null,
        $jsonBody = null,
        $httpHeaders = null
    ) {
        parent::__construct($message, $httpStatus, $httpBody, $jsonBody, $httpHeaders);
        $this->WLMStripeParam = $WLMStripeParam;
    }

    public function getWLMStripeParam()
    {
        return $this->WLMStripeParam;
    }
}
