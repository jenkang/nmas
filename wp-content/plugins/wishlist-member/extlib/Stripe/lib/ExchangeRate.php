<?php

namespace WLMStripe;

/**
 * Class ExchangeRate
 *
 * @package WLMStripe
 */
class ExchangeRate extends ApiResource
{

    const OBJECT_NAME = "exchange_rate";

    use ApiOperations\All;
    use ApiOperations\Retrieve;
}
