<?php
include $this->pluginDir . '/lib/integration.webinar.gotomeetingapi.php';

// Fetch webinar settings
$webinar_settings = $this->GetOption('webinar');

$obj = new WLM_GTMAPI_OAuth_En();
$oauth = new WLM_GTMAPI_OAuth($obj);


// If auth code is more than 10 then it's the new format for version 2 Oauth.
// Let's refresh the token as Access Token expires after 60 minutes.
if(isset($webinar_settings['gotomeetingapi']['authorizationcode']) && strlen($webinar_settings['gotomeetingapi']['authorizationcode']) > 10) {
	$gtm_api = new WishListMemberWebinarIntegrationGotowebinarApi();
	$gtm_api->refreshtoken();

	// Fetch again the refreshed Tokens
	$webinar_settings = $this->GetOption('webinar');
}

if(isset($_POST['savegtmapicode'])) {

	$data = $oauth->getAccessTokenv2(trim($_POST['authorizationcode']));

	if(isset($data->error)) {
		echo '<font color="red" size="6px">Sorry, incorrect Authorization Code</font>';
	} else {
		$_POST['webinar']['accesstoken'] = $data->access_token;
		$_POST['webinar']['organizerkey'] = $data->organizer_key;
		$_POST['webinar']['authorizationcode'] = trim($_POST['authorizationcode']);
		$_POST['webinar']['refreshtoken'] = $data->refresh_token;
		$webinar_settings[$webinar_provider] = $_POST['webinar'];

		$this->SaveOption('webinar', $webinar_settings);

		$webinars3 = $webinar_settings['gotomeetingapi'];

		$objOAuthEn = $oauth->getOAuthEntityClone();

		$obj->setAccessToken($webinars3['accesstoken']);
		$obj->setOrganizerKey($webinars3['organizerkey']);
		$webinars = $oauth->getWebinars();
	}
} else {
	
	$webinars3 = $webinar_settings['gotomeetingapi'];

	// Check if we updated the gtm webinar info and if the authorization code was changed
	if(isset($_POST['updategtmapi'])) {

		if(trim($_POST['txtauthorizationcode2']) != $webinars3['authorizationcode']) {

			$data = $oauth->getAccessTokenv2(trim($_POST['txtauthorizationcode2']));

			if(isset($data->error)) {
				echo '<font color="red" size="6px">Sorry, incorrect Authorization Code</font>';
			} else {
				$_POST['webinar']['accesstoken'] = $data->access_token;
				$_POST['webinar']['organizerkey'] = $data->organizer_key;
				$_POST['webinar']['authorizationcode'] = trim($_POST['txtauthorizationcode2']);
				$_POST['webinar']['refreshtoken'] = $data->refresh_token;
				$webinar_settings[$webinar_provider] = $_POST['webinar'];

				$this->SaveOption('webinar', $webinar_settings);

				$webinars3 = $webinar_settings['gotomeetingapi'];

				$objOAuthEn = $oauth->getOAuthEntityClone();

				$obj->setAccessToken($webinars3['accesstoken']);
				$obj->setOrganizerKey($webinars3['organizerkey']);
				$webinars = $oauth->getWebinars();
			}
		
		} else {
			$obj->setAccessToken($webinars3['accesstoken']);
			$obj->setOrganizerKey($webinars3['organizerkey']);
			$webinars = $oauth->getWebinars();
		}
	} else 	{
		// If it wasn't changed then we just fetch what's saved in the db
		$obj->setAccessToken($webinars3['accesstoken']);
		$obj->setOrganizerKey($webinars3['organizerkey']);
		$webinars = $oauth->getWebinars();
	}
}

// Check if the auth code is less than 10 characters, if it is then it's using the old auth_code format, display the error message.
if(isset($webinar_settings['gotomeetingapi']['authorizationcode']) && strlen($webinar_settings['gotomeetingapi']['authorizationcode']) < 10) {
	echo '<div class="error">';
	echo '<form method="post">';
	echo '<p style="color:red;font-weight:bold">Important: The previous Authentication method of GotoWebinar will be deprecated on August 14, 2018. This means that after the said date your GoToWebinar integration will stop working. <br>Please reauthenticate your GoToWebinar Integration by getting a new Authentication Code using <a target="_blank" href="'. $oauth->getApiAuthorizationUrl().'">this link </a> and paste it in the Authorization Code box below and then click the "Update Webinar Settings" button.</p>';
	echo '</form>';
	echo '</div>';
}

if(!$oauth->hasApiError()){
?>
<h2 class="wlm-integration-steps">Step 1: Obtain GoToWebinar API Authorization Code</h2>
<p><a target="_blank" href="<?php echo $oauth->getApiAuthorizationUrl(); ?>">
	Click here to obtain an authorization code then copy and paste it in the box below.
</a></p>
<form method="post">
	<label>Authorization Code: </label><input type="text" name="txtauthorizationcode2" size="50" value="<?php echo $webinars3['authorizationcode']; ?>">

	<h2 class="wlm-integration-steps">Step 2: Map your Membership Levels to your Webinars</h2>
	<p>Map your membership levels to your webinars by selecting a webinar from the dropdowns provided under the "Webinar" column.</p>
	<table class="widefat">
		<thead>
			<tr>
				<th scope="col" style="max-width:40%"><?php _e('Membership Level', 'wishlist-member'); ?></th>
				<th scope="col"><?php _e('Webinar', 'wishlist-member'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php 
			foreach ($wpm_levels AS $levelid => $level): 
			$webinar4 = explode('---', $webinars3[$levelid]);
			
				?>
				<tr class="<?php echo ++$webinar_row % 2 ? 'alternate' : ''; ?>">
					<th scope="row"><?php echo $level['name']; ?></th>
					<td align="left">
						<select name="webinar[gotomeetingapi][<?php echo $levelid; ?>]" />
							<option value="<?php echo $webinar4[0]; ?>---<?php echo $webinar4[1]; ?>"><?php echo $webinar4[1]; ?></option>
							<option> </option>
							<?php foreach($webinars as $key => $webinar): ?>
							<option value="<?php echo $webinar->webinarKey; ?>---<?php echo $webinar->subject; ?>"><?php echo $webinar->subject; ?> </option>
							<?php endforeach; ?>
						
						</select>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<p class="submit">
		<input type="hidden" name="updategtmapi">
		<input type="hidden" name="webinar[gotomeetingapi][authorizationcode]" value="<?php echo $webinars3['authorizationcode']; ?>">
		<input type="hidden" name="webinar[gotomeetingapi][accesstoken]" value="<?php echo $webinars3['accesstoken']; ?>">
		<input type="hidden" name="webinar[gotomeetingapi][organizerkey]" value="<?php echo $webinars3['organizerkey']; ?>">
		<input type="hidden" name="webinar[gotomeetingapi][refreshtoken]" value="<?php echo $webinars3['refreshtoken']; ?>">
		<input type="submit" class="button-primary" value="<?php _e('Update Webinar Settings', 'wishlist-member'); ?>" />
	</p>
</form>
<?php
}else{

?>
<form method="post">
	<p>
		<a target="_blank" href="<?php echo $oauth->getApiAuthorizationUrl(); ?>">
			Click Here to obtain an Authorization Code and paste it into the field below
		</a>
		<br><br>
		<label>Authorization Code: <input type="text" name="authorizationcode" size="50" value="<?php echo (isset($_POST['authorizationcode'])) ? $_POST['authorizationcode'] : ''; ?>">
	</p>
	<p class="submit">
		<input type="submit" name="savegtmapicode" class="button-primary" value="<?php _e('Save Settings', 'wishlist-member'); ?>" />
	</p>
</form>
<?php
}
?>
<div class="integration-links"
	data-video=""
	data-affiliate="http://wlplink.com/go/gotowebinar">
