<?php

/*
 * Stripe Integration Admin Interface
 * Original Author : Mike Lopez
 * Version: $Id: integration.shoppingcart.stripe.php 4495 2018-06-19 13:46:50Z mike $
 */
$__index__ = 'stripe';
$__sc_options__[$__index__] = 'Stripe';
$__sc_affiliates__[$__index__] = 'https://stripe.com/';
$__sc_videotutorial__[$__index__] = wlm_video_tutorial ( 'integration', 'sc', $__index__ );

define('MAX_PLAN_COUNT', 999);
define('MAX_PROD_COUNT', 999);
$currencies = array('USD','AED','AFN','ALL','AMD','ANG','AOA','ARS','AUD','AWG','AZN','BAM','BBD','BDT','BGN','BIF','BMD','BND','BOB','BRL','BSD','BWP','BZD','CAD','CDF','CHF','CLP','CNY','COP','CRC','CVE','CZK','DJF','DKK','DOP','DZD','EEK','EGP','ETB','EUR','FJD','FKP','GBP','GEL','GIP','GMD','GNF','GTQ','GYD','HKD','HNL','HRK','HTG','HUF','IDR','ILS','INR','ISK','JMD','JPY','KES','KGS','KHR','KMF','KRW','KYD','KZT','LAK','LBP','LKR','LRD','LSL','LTL','LVL','MAD','MDL','MGA','MKD','MNT','MOP','MRO','MUR','MVR','MWK','MXN','MYR','MZN','NAD','NGN','NIO','NOK','NPR','NZD','PAB','PEN','PGK','PHP','PKR','PLN','PYG','QAR','RON','RSD','RUB','RWF','SAR','SBD','SCR','SEK','SGD','SHP','SLL','SOS','SRD','STD','SVC','SZL','THB','TJS','TOP','TRY','TTD','TWD','TZS','UAH','UGX','UYU','UZS','VEF','VND','VUV','WST','XAF','XCD','XOF','XPF','YER','ZAR','ZMW');

if (wlm_arrval($_GET, 'cart') == $__index__) {
	if (!$__INTERFACE__) {
		// BEGIN Initialization
		$stripethankyou = $this->GetOption('stripethankyou');
		if (!$stripethankyou) {
			$this->SaveOption('stripethankyou', $stripethankyou = $this->MakeRegURL());
		}

		// save POST URL
		if (wlm_arrval($_POST, 'stripethankyou')) {
			$_POST['stripethankyou'] = trim(wlm_arrval($_POST, 'stripethankyou'));
			$wpmx = trim(preg_replace('/[^A-Za-z0-9]/', '', $_POST['stripethankyou']));
			if ($wpmx == $_POST['stripethankyou']) {
				if ($this->RegURLExists($wpmx, null, 'stripethankyou')) {
					echo "<div class='error fade'>" . __('<p><b>Error:</b> stripe Thank You URL (' . $wpmx . ') is already in use by a Membership Level or another Shopping Cart.  Please try a different one.</p>', 'wishlist-member') . "</div>";
				} else {
					$this->SaveOption('stripethankyou', $stripethankyou = $wpmx);
					echo "<div class='updated fade'>" . __('<p>Thank You URL Changed.&nbsp; Make sure to update stripe with the same Thank You URL to make it work.</p>', 'wishlist-member') . "</div>";
				}
			} else {
				echo "<div class='error fade'>" . __('<p><b>Error:</b> Thank You URL may only contain letters and numbers.</p>', 'wishlist-member') . "</div>";
			}
		}
		if (isset($_POST['stripeapikey'])) {
			$stripeapikey = trim($_POST['stripeapikey']);
			$this->SaveOption('stripeapikey', $stripeapikey);
		}

		if (isset($_POST['stripepublishablekey'])) {
			$stripepublishablekey = trim($_POST['stripepublishablekey']);
			$this->SaveOption('stripepublishablekey', $stripepublishablekey);
		}

		if (wlm_arrval($_POST, 'stripeconnections')) {
			$stripeconnections = $_POST['stripeconnections'];
			$this->SaveOption('stripeconnections', $stripeconnections);
		}

		if (isset($_POST['stripesettings'])) {
			$stripesettings = $_POST['stripesettings'];
			$this->SaveOption('stripesettings', $stripesettings);
		}

		$stripethankyou_url = $wpm_scregister . $stripethankyou;
		$stripeapikey = $this->GetOption('stripeapikey');
		$stripepublishablekey = $this->GetOption('stripepublishablekey');
		$stripeconnections = $this->GetOption('stripeconnections');
		$stripesettings = $this->GetOption('stripesettings');
		// END Initialization
	} else {
		if(!class_exists('WLMStripe\WLM_Stripe')) return false;

		include_once($this->pluginDir . '/resources/forms/integration.shoppingcart.stripe.interface.php');

		
	}
}
?>
