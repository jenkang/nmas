<?php
	global $wp_version;
	list($current_version) = explode('-', $wp_version);
	$wp_version_ok = version_compare($current_version, '4.9.6', '>=');
?>
<h2 style="display:none"></h2>
<?php switch(wlm_arrval($_GET, 'mode2')) : 
	case 'emailtemplates': ?>
		<?php
			if(!$wp_version_ok) {
				echo '<p>This section requires WordPress 4.9.6 or higher</p>';
				continue;
			}
		?>
		<form method="post" id="privacy_email_templates_reset">
			<input type="hidden" name="WishListMemberAction" value="ResetPrivacyEmailTemplates" />
			<input type="hidden" name="target" value="" />
		</form>
		<form method="post" id="privacy_email_templates">
			<h2>User Request Email</h2>
			<table class="form-table">
				<tr>
					<th>Subject:</th>
					<td><input size="50" type="text" name="<?php $this->Option('privacy_email_template_request_subject'); ?>" value="<?php $this->OptionValue(); ?>" /></td>
				</tr>
				<tr>
					<th>Body:</th>
					<td>
						<textarea cols="50" rows="15" name="<?php $this->Option($x = 'privacy_email_template_request'); ?>" id=<?php echo $x; ?>><?php $this->OptionValue(); ?></textarea>
						<p><a href="#" class="reset-email-template" data-target="privacy_email_template_request">Reset Email Template</a></p>
						<ul id="nav" class="merge-code-inserter">
							<li><a href="javascript:return false;" style="text-decoration: none"> <img src="<?php echo $this->pluginURL.'/images/WishList-Icon-Blue-16.png'; ?>"> Merge codes </a>
								<ul class="nav-2" style="display: none">
									<li><a href="javascript:;" onclick="wpm_insertHTML('[firstname]','<?php echo $x; ?>')"><?php _e('Insert First Name', 'wishlist-member'); ?></a> </li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[lastname]','<?php echo $x; ?>')"><?php _e('Insert Last Name', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[email]','<?php echo $x; ?>')"><?php _e('Insert Email', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[username]','<?php echo $x; ?>')"><?php _e('Insert Username', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[request]','<?php echo $x; ?>')"><?php _e('Insert Request Description', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[confirm_url]','<?php echo $x; ?>')"><?php _e('Insert Confirmation URL', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[sitename]','<?php echo $x; ?>')"><?php _e('Insert Site Name', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[siteurl]','<?php echo $x; ?>')"><?php _e('Insert Site URL', 'wishlist-member'); ?></a></li>
								</ul>
							</li>
						</ul>
						<br clear="all" />
					</td>
				</tr>
			</table>
			<h2>Download Fulfilled</h2>
			<table class="form-table">
				<tr>
					<th>Subject:</th>
					<td><input size="50" type="text" name="<?php $this->Option('privacy_email_template_download_subject'); ?>" value="<?php $this->OptionValue(); ?>" /></td>
				</tr>
				<tr>
					<th>Body:</th>
					<td>
						<textarea cols="50" rows="15" name="<?php $this->Option($x = 'privacy_email_template_download'); ?>" id="<?php echo $id; ?>"><?php $this->OptionValue(); ?></textarea>
						<p><a href="#" class="reset-email-template" data-target="privacy_email_template_download">Reset Email Template</a></p>
						<ul id="nav" class="merge-code-inserter">
							<li><a href="javascript:return false;" style="text-decoration: none"> <img src="<?php echo $this->pluginURL.'/images/WishList-Icon-Blue-16.png'; ?>"> Merge codes </a>
								<ul class="nav-2" style="display: none">
									<li><a href="javascript:;" onclick="wpm_insertHTML('[firstname]','<?php echo $x; ?>')"><?php _e('Insert First Name', 'wishlist-member'); ?></a> </li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[lastname]','<?php echo $x; ?>')"><?php _e('Insert Last Name', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[email]','<?php echo $x; ?>')"><?php _e('Insert Email', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[username]','<?php echo $x; ?>')"><?php _e('Insert Username', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[expiration]','<?php echo $x; ?>')"><?php _e('Insert Expiration Date', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[link]','<?php echo $x; ?>')"><?php _e('Insert Download Link', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[sitename]','<?php echo $x; ?>')"><?php _e('Insert Site Name', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[siteurl]','<?php echo $x; ?>')"><?php _e('Insert Site URL', 'wishlist-member'); ?></a></li>
								</ul>
							</li>
						</ul>
						<br clear="all" />
					</td>
				</tr>
			</table>
			<h2>Erasure Fulfilled</h2>
			<table class="form-table">
				<tr>
					<th>Subject:</th>
					<td><input size="50" type="text" name="<?php $this->Option('privacy_email_template_delete_subject'); ?>" value="<?php $this->OptionValue(); ?>" /></td>
				</tr>
				<tr>
					<th>Body:</th>
					<td>
						<textarea cols="50" rows="15" name="<?php $this->Option($x = 'privacy_email_template_delete'); ?>" id="<?php echo $x; ?>"><?php $this->OptionValue(); ?></textarea>
						<p><a href="#" class="reset-email-template" data-target="privacy_email_template_delete">Reset Email Template</a></p>
						<ul id="nav" class="merge-code-inserter">
							<li><a href="javascript:return false;" style="text-decoration: none"> <img src="<?php echo $this->pluginURL.'/images/WishList-Icon-Blue-16.png'; ?>"> Merge codes </a>
								<ul class="nav-2" style="display: none">
									<li><a href="javascript:;" onclick="wpm_insertHTML('[firstname]','<?php echo $x; ?>')"><?php _e('Insert First Name', 'wishlist-member'); ?></a> </li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[lastname]','<?php echo $x; ?>')"><?php _e('Insert Last Name', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[email]','<?php echo $x; ?>')"><?php _e('Insert Email', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[username]','<?php echo $x; ?>')"><?php _e('Insert Username', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[sitename]','<?php echo $x; ?>')"><?php _e('Insert Site Name', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[siteurl]','<?php echo $x; ?>')"><?php _e('Insert Site URL', 'wishlist-member'); ?></a></li>
								</ul>
							</li>
						</ul>
						<br clear="all" />

					</td>
				</tr>
			</table>
			<p class="submit">
				<?php $this->Options();
				$this->RequiredOptions();
				?>
				<input type="hidden" name="WishListMemberAction" value="Save" />
				<input type="submit" class="button-primary" value="<?php _e('Save', 'wishlist-member'); ?>" />
			</p>
		</form>
	<?php break; ?>
	<?php case 'managedata': ?>
		<?php
			if(!$wp_version_ok) {
				echo '<p>This section requires WordPress 4.9.6 or higher</p>';
				continue;
			}
		?>
		<p>This functionality is handled directly by WordPress.</p>
		<blockquote>
			<ul>
				<li><a href="<?php echo admin_url('tools.php?page=export_personal_data'); ?>">Export Personal Data</a></li>
				<li><a href="<?php echo admin_url('tools.php?page=remove_personal_data'); ?>">Erase Personal Data</a></li>
			</ul>
		</blockquote>
	<?php break; ?>
	<?php case 'unsubscribednotification': ?>
		<?php
			if(!$wp_version_ok) {
				echo '<p>This section requires WordPress 4.9.6 or higher</p>';
				continue;
			}
		?>
		<form method="post" id="privacy_email_templates_reset">
			<input type="hidden" name="WishListMemberAction" value="ResetPrivacyEmailTemplates" />
			<input type="hidden" name="target" value="" />
		</form>
		<form method="post" id="privacy_email_templates">
			<table class="form-table">
				<tr>
					<td colspan="2" style="padding-left: 0">
						<label>
							<input type="checkbox" name="<?php $this->Option('member_unsub_notification'); ?>" value="1"<?php $this->OptionChecked(1); ?> />
							<?php _e('Notify a member when they are unsubscribed from Email Broadcast mailing list', 'wishlist-member'); ?>
						</label>
					</td>
				</tr>
				<tr class="unsubscribed-notification-enabled">
					<th>Subject:</th>
					<td><input size="50" type="text" name="<?php $this->Option('member_unsub_notification_subject'); ?>" value="<?php $this->OptionValue(); ?>" /></td>
				</tr>
				<tr class="unsubscribed-notification-enabled">
					<th>Body:</th>
					<td>
						<textarea cols="50" rows="15" name="<?php $this->Option($x = 'member_unsub_notification_body'); ?>" id=<?php echo $x; ?>><?php $this->OptionValue(); ?></textarea>
						<p><a href="#" class="reset-email-template" data-target="member_unsub_notification_body">Reset Email Template</a></p>
						<ul id="nav" class="merge-code-inserter">
							<li><a href="javascript:return false;" style="text-decoration: none"> <img src="<?php echo $this->pluginURL.'/images/WishList-Icon-Blue-16.png'; ?>"> Merge codes </a>
								<ul class="nav-2" style="display: none">
									<li><a href="javascript:;" onclick="wpm_insertHTML('[firstname]','<?php echo $x; ?>')"><?php _e('Insert First Name', 'wishlist-member'); ?></a> </li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[lastname]','<?php echo $x; ?>')"><?php _e('Insert Last Name', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[email]','<?php echo $x; ?>')"><?php _e('Insert Email', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[username]','<?php echo $x; ?>')"><?php _e('Insert Username', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[sitename]','<?php echo $x; ?>')"><?php _e('Insert Site Name', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[siteurl]','<?php echo $x; ?>')"><?php _e('Insert Site URL', 'wishlist-member'); ?></a></li>
									<li><a href="javascript:;" onclick="wpm_insertHTML('[resubscribeurl]','<?php echo $x; ?>')"><?php _e('Insert Subscribe URL', 'wishlist-member'); ?></a></li>
								</ul>
							</li>
						</ul>
						<br clear="all" />
					</td>
				</tr>
			</table>
			<p class="submit">
				<?php $this->Options();
				$this->RequiredOptions();
				?>
				<input type="hidden" name="WishListMemberAction" value="Save" />
				<input type="submit" class="button-primary" value="<?php _e('Save', 'wishlist-member'); ?>" />
			</p>
		</form>
	<?php break; ?>
	<?php default: ?>
		<?php
			$pages = get_pages('exclude=' . implode(',', $this->ExcludePages(array(), true)));
			foreach($pages AS &$page) {
				$page = sprintf('<option value="%d">%s</option>', $page->ID, $page->post_title);
			}
			unset($page);
			array_unshift($pages, '<option value="0">- None -</option>');
		?>
		<form method="post">
			<h2>Terms of Service Agreement</h2>
			<table class="form-table">
				<tr valign="top">
					<td colspan="2" style="padding-left: 0">
						<label>
							<input type="checkbox" name="<?php $this->Option('privacy_require_tos_on_registration'); ?>" value="1"<?php $this->OptionChecked(1); ?> />
							<?php _e('Require Terms of Service Agreement on Registration Form', 'wishlist-member'); ?>
						</label>
					</td>
				</tr>
				<tr valign="top" class="require-tos-enabled">
					<th scope="row"><?php _e('Terms of Service Checkbox Text:', 'wishlist-member'); ?></th>
					<td>
						<textarea name="<?php $this->Option('privacy_require_tos_checkbox_text'); ?>" cols="50" rows="5"><?php $this->OptionValue(); ?></textarea>
					</td>
				</tr>
				<tr valign="top" class="require-tos-enabled">
					<th scope="row"><?php _e('Error Message:', 'wishlist-member'); ?></th>
					<td>
						<textarea name="<?php $this->Option('privacy_require_tos_error_message'); ?>" cols="50" rows="5"><?php $this->OptionValue(); ?></textarea>
					</td>
				</tr>
			</table>

			<h2>Additional Marketing Consent</h2>
			<table class="form-table">
				<tr valign="top">
					<td colspan="2" style="padding-left: 0">
						<label>
							<input type="checkbox" name="<?php $this->Option('privacy_enable_consent_to_market'); ?>" value="1"<?php $this->OptionChecked(1); ?> />
							<?php _e('Display Consent Checkbox for Additional Marketing on Registration Form', 'wishlist-member'); ?>
						</label>
					</td>
				</tr>
				<tr valign="top" class="consent-enabled">
					<th scope="row"><?php _e('Consent Checkbox Text:', 'wishlist-member'); ?></th>
					<td>
						<textarea name="<?php $this->Option('privacy_consent_to_market_text'); ?>" cols="50" rows="5"><?php $this->OptionValue(); ?></textarea>
					</td>
				</tr>
				<tr valign="top" class="consent-enabled">
					<th scope="row"><?php _e('Consent Checkbox affects the following:', 'wishlist-member'); ?></th>
					<td>
						<label>
							<input type="checkbox" name="<?php $this->Option('privacy_consent_affects_emailbroadcast'); ?>" value="1"<?php $this->OptionChecked(1); ?> />
							<?php _e('Email Broadcast', 'wishlist-member'); ?>
						</label>
						<br>
						<label>
							<input type="checkbox" name="<?php $this->Option('privacy_consent_affects_autoresponder'); ?>" value="1"<?php $this->OptionChecked(1); ?> />
							<?php _e('Auto-Responder', 'wishlist-member'); ?>
						</label>
						<br>
					</td>
				</tr>

			</table>

			<h2>Legal Pages</h2>
			<table class="form-table">
				<tr valign="top">
					<td colspan="2" style="padding-left: 0">
						<label>
							<input type="checkbox" name="<?php $this->Option('privacy_display_tos_on_footer'); ?>" value="1"<?php $this->OptionChecked(1); ?> />
							<?php _e('Display Terms of Service Link on Site Footer', 'wishlist-member'); ?>
						</label>
					</td>
				</tr>
				<tr valign="top" class="tos-enabled">
					<th scope="row"><?php _e('Terms of Service Page:', 'wishlist-member'); ?></th>
					<td>
						<select name="<?php $this->Option('privacy_tos_page'); ?>" initial-value="<?php $this->OptionValue(); ?>">
							<?php echo implode('', $pages); ?>
						</select>
					</td>
				</tr>
				<tr valign="top">
					<td colspan="2" style="padding-left: 0">
						<label>
							<input type="checkbox" name="<?php $this->Option('privacy_display_pp_on_footer'); ?>" value="1"<?php $this->OptionChecked(1); ?> />
							<?php _e('Display Privacy Policy Link on Site Footer', 'wishlist-member'); ?>
						</label>
					</td>
				</tr>
				<tr valign="top" class="pp-enabled">
					<th scope="row"><?php _e('Privacy Policy Page:', 'wishlist-member'); ?></th>
					<td>
						<select name="<?php $this->Option('privacy_pp_page'); ?>" initial-value="<?php $this->OptionValue(); ?>">
							<?php echo implode('', $pages); ?>
						</select>
					</td>
				</tr>
			</table>
			<p class="submit">
				<?php $this->Options();
				$this->RequiredOptions();
				?>
				<input type="hidden" name="WishListMemberAction" value="Save" />
				<input type="submit" class="button-primary" value="<?php _e('Save', 'wishlist-member'); ?>" />
			</p>
		</form>
<?php endswitch; ?>

<script type="text/javascript">
	jQuery(function($) {
		$('input[name=privacy_require_tos_on_registration]').on('change', function() {
			if($('input[name=privacy_require_tos_on_registration]:checked').val() == 1) {
				$('.require-tos-enabled').show();
			} else {
				$('.require-tos-enabled').hide();
			}
		}).filter(':checked').trigger('change');

		$('input[name=privacy_enable_consent_to_market]').on('change', function() {
			if($('input[name=privacy_enable_consent_to_market]:checked').val() == 1) {
				$('.consent-enabled').show();
			} else {
				$('.consent-enabled').hide();
			}
		}).filter(':checked').trigger('change');

		$('input[name=privacy_display_tos_on_footer]').on('change', function() {
			if($('input[name=privacy_display_tos_on_footer]:checked').val() == 1) {
				$('.tos-enabled').show();
			} else {
				$('.tos-enabled').hide();
			}
		}).filter(':checked').trigger('change');

		$('input[name=privacy_display_pp_on_footer]').on('change', function() {
			if($('input[name=privacy_display_pp_on_footer]:checked').val() == 1) {
				$('.pp-enabled').show();
			} else {
				$('.pp-enabled').hide();
			}
		}).filter(':checked').trigger('change');

		$('input[name=member_unsub_notification]').on('change', function() {
			if($('input[name=member_unsub_notification]:checked').val() == 1) {
				$('.unsubscribed-notification-enabled').show();
			} else {
				$('.unsubscribed-notification-enabled').hide();
			}
		}).filter(':checked').trigger('change');

		$('select[initial-value]').each(function() {
			$(this).val($(this).attr('initial-value'));
		});

		$('.merge-code-inserter').on('mouseover', function() {
			$(this).find('ul').show();
		});
		$('.merge-code-inserter').on('mouseout', function() {
			$(this).find('ul').hide();
		});

		$('.reset-email-template').on('click', function() {
			if(confirm('Are you sure?')) {
				$('form#privacy_email_templates_reset input[name=target]').val($(this).data('target'));
				$('form#privacy_email_templates_reset').submit();
			}
			return false;
		});
	});
</script>
<style type="text/css">
	.consent-enabled, .require-tos-enabled, .tos-enabled, .pp-enabled, .unsubscribed-notification-enabled {
		display: none;
	}
	.merge-code-inserter ul {
		background: #EBEAEB;
		padding: 5px;
		border: 1px solid #ccc;
		margin-left: 10px;
	}
	.merge-code-inserter img {
		vertical-align: middle;
	}
	textarea {
		float:left;
		margin-right: 10px;		
	}
</style>