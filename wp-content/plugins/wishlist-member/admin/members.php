<?php if ( isset($show_page_menu) && $show_page_menu) : ?>
	<ul class="wlm-sub-menu with-sub-two">
		<?php if ($this->access_control->current_user_can('wishlistmember_members_manage')): ?>
			<li<?php echo (!wlm_arrval($_GET,'mode')) ? ' class="current"' : '' ?>><a href="?<?php echo $this->QueryString('usersearch', 'mode', 'level') ?>"><?php _e('Manage Members', 'wishlist-member'); ?></a></li>
		<?php endif; ?>

		<?php if ($this->access_control->current_user_can('wishlistmember_members_import')): ?>
			<li<?php echo (wlm_arrval($_GET,'mode') == 'import') ? ' class="current"' : '' ?>><a href="?<?php echo $this->QueryString('usersearch', 'mode', 'level') ?>&mode=import"><?php _e('Import', 'wishlist-member'); ?></a></li>
		<?php endif; ?>

		<?php if ($this->access_control->current_user_can('wishlistmember_members_export')): ?>
			<li<?php echo (wlm_arrval($_GET,'mode') == 'export') ? ' class="current"' : '' ?>><a href="?<?php echo $this->QueryString('usersearch', 'mode', 'level') ?>&mode=export"><?php _e('Export', 'wishlist-member'); ?></a></li>
		<?php endif; ?>

		<?php if ($this->access_control->current_user_can('wishlistmember_members_broadcast')): ?>
			<li<?php echo (wlm_arrval($_GET,'mode') == 'broadcast' || wlm_arrval($_GET,'mode') == 'sendbroadcast') ? ' class="current"' : '' ?>><a href="?<?php echo $this->QueryString('usersearch', 'mode', 'level') ?>&mode=broadcast"><?php _e('Email Broadcast', 'wishlist-member'); ?></a></li>
		<?php endif; ?>

		<?php if ($this->access_control->current_user_can('wishlistmember_members_blacklist')): ?>
			<li<?php echo (wlm_arrval($_GET,'mode') == 'blacklist') ? ' class="current"' : '' ?>><a href="?<?php echo $this->QueryString('usersearch', 'mode', 'level') ?>&mode=blacklist"><?php _e('Blacklist', 'wishlist-member'); ?></a></li>
		<?php endif; ?>

		<?php if ($this->access_control->current_user_can('wishlistmember_members_dataprivacy')): ?>
			<li<?php echo (wlm_arrval($_GET,'mode') == 'dataprivacy') ? ' class="current has-sub-menu"' : '' ?>><a href="?<?php echo $this->QueryString('usersearch', 'mode', 'level') ?>&mode=dataprivacy"><?php _e('Data Privacy', 'wishlist-member'); ?></a></li>
		<?php endif; ?>
	</ul>
	<?php if (wlm_arrval($_GET, 'mode') == 'dataprivacy') : ?>
		<?php $base_url = sprintf('page=%s&wl=%s&mode=%s', wlm_arrval($_GET, 'page'), wlm_arrval($_GET, 'wl'), wlm_arrval($_GET, 'mode')); ?>
		<ul class="wlm-sub-menu sub-sub">
			<li<?php echo (!wlm_arrval($_GET,'mode2')) ? ' class="current"' : '' ?>><a href="?<?php echo $base_url; ?>"><?php _e('Settings', 'wishlist-member'); ?></a></li>
			<li<?php echo (wlm_arrval($_GET,'mode2') == 'emailtemplates') ? ' class="current"' : '' ?>><a href="?<?php echo $base_url; ?>&mode2=emailtemplates"><?php _e('Data Request Templates', 'wishlist-member'); ?></a></li>
			<li<?php echo (wlm_arrval($_GET,'mode2') == 'managedata') ? ' class="current"' : '' ?>><a href="?<?php echo $base_url; ?>&mode2=managedata"><?php _e('Manage User Data', 'wishlist-member'); ?></a></li>
			<li<?php echo (wlm_arrval($_GET,'mode2') == 'unsubscribednotification') ? ' class="current"' : '' ?>><a href="?<?php echo $base_url; ?>&mode2=unsubscribednotification"><?php _e('Unsubscribe Notification', 'wishlist-member'); ?></a></li>
		</ul>
	<?php endif; ?>
	<?php return;
endif;
?>
<?php
// Get Membership Levels
$wpm_levels = $this->GetOption('wpm_levels');
if ($_POST) {
	foreach ((array) $_POST as $pk => $pv) {
		if (!is_array($pv))
			$_POST[$pk] = trim($pv);
	}
}
?>
<?php
if (wlm_arrval($_GET,'mode') == 'blacklist') {
	include('members.blacklist.php');
	include_once($this->pluginDir . '/admin/tooltips/members.blacklist.tooltips.php');
} elseif (wlm_arrval($_GET,'mode') == 'broadcast') {
	include('members.broadcast.php');
	include_once($this->pluginDir . '/admin/tooltips/members.broadcast.tooltips.php');
} elseif (wlm_arrval($_GET,'mode') == 'sendbroadcast') {
	include('members.sendbroadcast.php');
	include_once($this->pluginDir . '/admin/tooltips/members.broadcast.tooltips.php');
} elseif (wlm_arrval($_GET,'mode') == 'import') {
	include('members.import.php');
	include_once($this->pluginDir . '/admin/tooltips/members.import.tooltips.php');
} elseif (wlm_arrval($_GET,'mode') == 'export') {
	include('members.export.php');
	include_once($this->pluginDir . '/admin/tooltips/members.export.tooltips.php');
} elseif (wlm_arrval($_GET,'mode') == 'dataprivacy') {
	include('members.privacy.php');
} else {
	include('members.default.php');
	include_once($this->pluginDir . '/admin/tooltips/members.default.tooltips.php');
}
?>