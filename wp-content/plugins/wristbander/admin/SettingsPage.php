<?php
class WristbanderSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Wristbander Settings', 
            'Wristbander', 
            'manage_options', 
            'wristbander', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'wristbander_options' );
        ?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2>Wristbander Settings</h2>           
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'wristbander_settings_group' );   
                do_settings_sections( 'wristbander' );
                submit_button(); 
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'wristbander_settings_group', // Option group
            'wristbander_options', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'wristbander_settings', // ID
            'System Pages', // Title
            array( $this, 'print_section_info' ), // Callback
            'wristbander' // Page
        );  

        add_settings_field(
            'generate_page', // ID
            'Select the generate page: ', // Title 
            array( $this, 'generate_page_callback' ), // Callback
            'wristbander', // Page
            'wristbander_settings' // Section           
        );      

        add_settings_field(
            'manage_page', 
            'Select the manage page:', 
            array( $this, 'manage_page_callback' ), 
            'wristbander', 
            'wristbander_settings'
        );      
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['generate_page'] ) )
            $new_input['generate_page'] = absint( $input['generate_page'] );

        if( isset( $input['manage_page'] ) )
            $new_input['manage_page'] = absint( $input['manage_page'] );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Use the options below to select the system pages used throughout the Wristbander plugin. This is necessary for the links and redirects inside Wristbander.';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function generate_page_callback()
    {
		$pages = get_pages();
		
        printf( '<select id="generate_page" name="wristbander_options[generate_page]">' );
		
		foreach ( $pages as $page ) {
			printf( '<option value="%s" %s>%s</option>', $page->ID, selected($page->ID, $this->options['generate_page'], false ), $page->post_title );
		}
		
		printf( '</select>' );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function manage_page_callback()
    {
        $pages = get_pages();
		
        printf( '<select id="manage_page" name="wristbander_options[manage_page]">' );
		
		foreach ( $pages as $page ) {
			printf( '<option value="%s" %s>%s</option>', $page->ID, selected($page->ID, $this->options['manage_page'], false ), $page->post_title );
		}
		
		printf( '</select>' );
    }
}

if( is_admin() )
    $WristbanderSettingsPage = new WristbanderSettingsPage();
