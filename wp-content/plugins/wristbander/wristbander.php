<?php
/* 
Plugin Name: NeverMissASign 1.0.1
Version: 1.0.1
Plugin URI: http://nevermissasign.com
Description: Plugin for creating wristbands [wb_generate] [wb_manage]
Author: NeverMissASign
Author URI: http://nevermissasign.com
License: GPL v3
 */

require_once( dirname( __FILE__ ) . '/admin/SettingsPage.php' );

if ( ! class_exists( 'Wristbander' ) ) {
	class Wristbander {		
		var $objects_table;
		var $id;
		var $open;
		var $delete;
		var $current_user;
		var $manage_url;
		var $generate_url;
		var $nonce_name = 'wristbander_nonce';
		var $nonce_action = 'wb_nonceit';
		var $options;
		
		public function __construct() {
                        global $wpdb;
                        
			// Properties
                        $this->objects_table = $wpdb->prefix . 'saved';
			$this->id = (int) @$_GET['id'];
			$this->open = @$_GET['open'];
			$this->edit = @$_GET['edit'];
			$this->delete = @$_GET['delete'];
			
			// Activation
			register_activation_hook( __FILE__, array( $this, 'activate' ) );
			
			// Init
			add_action( 'init', array( $this, 'init' ) );
			
			// Register js and css
			add_action( 'wp_enqueue_scripts', array( $this, 'register' ) );
			
			// Register shortcodes
			add_shortcode( 'wb_generate', array( $this, 'generate' ) );
			add_shortcode( 'wb_manage', array( $this, 'manage' ) );
			
			// Actions
			add_action( 'template_redirect', array( $this, 'process' ) );
		}
		public function activate() {
			$this->db_tables();
		}
		public function init() {
			global $current_user;
			
			$this->current_user = $current_user;
			$this->options = get_option( 'wristbander_options' );
			$this->manage_url = get_permalink( $this->options['manage_page'] );
			$this->generate_url = get_permalink( $this->options['generate_page'] );
		}
		public function register() {
			// Register js scripts
			wp_register_script( 'jquery.wristbander', plugins_url( '/js/jquery.wristbander.js', __FILE__ ), array( 'jquery', 'jquery-ui-core', 'jquery-ui-slider', 'jquery-touch-punch' ), 1.2 );
			
			// Register styles
			wp_register_style( 'jquery.ui.css', '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css' );
			wp_register_style( 'wristbander', plugins_url( 'css/style.css', __FILE__ ) );
			
			wp_enqueue_style( 'jquery.ui.css' );
			wp_enqueue_script( 'jquery.wristbander' );
			wp_enqueue_style( 'wristbander' );
		}
		public function generate() {
			if ( ! is_user_logged_in() ) {
				return;
			}			
			
			return $this->generate_html();
		}
		public function manage() {	
			if ( ! is_user_logged_in() ) {
				return;
			}
			
			if ( $this->id ) {
				if ( $this->open ) {
					return $this->manage_open();
				}
			} else {
				return $this->manage_list();
			}			
		}
		/***********************************************************************
		 * Internal Helpers
		 **********************************************************************/
		private function db_tables() {
			global $wpdb;

			$structures=array(
				"CREATE TABLE IF NOT EXISTS `{$this->objects_table}` (
					`ID` int(10) AUTO_INCREMENT PRIMARY KEY,
					`name` varchar(255) NOT NULL,
					`date` datetime NOT NULL,
					`grid` tinyint(3) NOT NULL DEFAULT 5,
					`data5` text NOT NULL,
					`data4` text NOT NULL,
					`data3` text NOT NULL,
					`data2` text NOT NULL,
					`data` text NOT NULL,
					`user` int(10) NOT NULL,
					`active` int(1) NOT NULL DEFAULT 1,
					`color` varchar(32) NULL DEFAULT NULL,
					`sets` int(11) NOT NULL,
					`fba` text NULL)"
			);
				
			foreach($structures AS $structure){
				$wpdb->query($structure);
			}
		}
		public function process() {
			if ( ! isset( $_POST['wb_action'] ) ) {
				$this->process_manage();
				
				return;
			}
			
			$action = $_POST['wb_action'];
			
			if ( $action == 'add_new' ) {
				$this->process_generate();
			}
		}
		private function process_generate() {
			if ( !wp_verify_nonce($_POST[$this->nonce_name], $this->nonce_action) ) {
				wp_die("Cheatin' uh?");
			}
			
			global $wpdb, $current_user;
				
			$sets = $_POST['sets'];
			
			if($sets>10) $sets=10;

			$total = count($_POST['names']);		

			if($this->id) {
				$sets=1;
			}

			for($l=0; $l < $sets; $l++) {

				$times = array();	
				$times2 = array();	

				$shared=150;
				$total_count=0;

				for($i=0; $i<count($_POST['fba']); $i++) {	
					$val = $_POST['fba'][$i];
					$total_count+=$val;
				}

				for($i=0; $i<count($_POST['fba']); $i++) {	
					$val = $_POST['fba'][$i];
					$percent = $val/$total_count;
					$num = floor($shared * $percent);
					for($x=0; $x<$num; $x++) {
						$times2[] = $_POST['names'][$i];
					}
				}

				$max = count($_POST['names']) - 1;

				while(count($times2)<150) {
					$i = rand(0, $max);
					$times2[] = $_POST['names'][$i];
					// add random players to fill
				}		

				shuffle($times2);

				$counter=0;
				$data = json_encode($times2);

				if($l>0) {
					$name .= "  #" . ($l+1); 
				}

				$fba  = json_encode($_POST['fba']);
				$data3 = json_encode($_POST['names']);
				$data2 = json_encode($_POST['names2']);

				if($this->id) {
					$wpdb->update( 
						$this->objects_table, 
						array( 
							'fba'		=>	$fba,
							'name'		=>	$_POST['name'] . ' ' . $name,
							'date'		=>	current_time('mysql', 1),
							'data3'		=>	$data3,
							'data2'		=>	$data2,
							'data'		=>	$data,
							'user'		=>	$current_user->ID,
							'active'	=>	1,
							'color'		=>	$_POST['color'],
							'sets'		=>	$_POST['sets']
						), 
						array( 'ID' => $this->id ), 
						array( 
							'%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s', '%s', '%s',
						), 
						array( '%d' ) 
					);
					
				} else {
					$wpdb->insert( 
						$this->objects_table, 
						array( 
							'fba'		=>	$fba,
							'name'		=>	$_POST['name'] . ' ' . $name,
							'date'		=>	current_time('mysql', 1),
							'data3'		=>	$data3,
							'data2'		=>	$data2,
							'data'		=>	$data,
							'user'		=>	$current_user->ID,
							'active'	=>	1,
							'color'		=>	$_POST['color'],
							'sets'		=>	$_POST['sets']
						), 
						array( 
							'%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s', '%s', '%s',
						) 
					);
				}
				
				unset($name);
			}

			wp_redirect($this->manage_url);
			exit();
		}
		private function generate_html() {
			global $wpdb;
			
			if($this->id) { 
				$row = $wpdb->get_row("SELECT * FROM {$this->objects_table} WHERE user='" . $this->current_user->ID . "' AND ID='" . $this->id . "'", ARRAY_A);
				
				if ( ! $row ) {
					return '<p class="error">Sorry, this wristband doesn\'t exist.</p>';
				}
			} 
			
			if(!$row['fba']) $row['fba']=100;
			if(!$row['fbi']) $row['fbi']=100;
			if(!$row['ch']) $row['cb']=100;
			if(!$row['sl']) $row['sl']=100;
			if(!$row['ch']) $row['ch']=100;
			if(!$row['p']) $row['p']=100;
			
			$fba = array();

			if($this->id) {
				$names  = json_decode($row['data3']);
				$names2 = json_decode($row['data2']);
				$fba = json_decode($row['fba']);

			} else { 
				$data = array();
				$names = array("FBA");
				$names2 = array("Fastball Away");
			}

			$row['fba']=100;
			$total=0;

			for($i=0; $i<count($names); $i++) {
				$value = $fba[$i];

				if(!$this->id || !$fba[$i]) {
					$value = 100;
				} 

				$total += $value;
			}
			
			ob_start(); ?>

			<div id="wb_generate">
				<form action="" method="POST">
					<input type="hidden" name="wb_action" value="add_new" />
					<?php wp_nonce_field($this->nonce_action, $this->nonce_name ); ?>
					<h4>Name</h4>
					<div class="form-field">
						<input type="text" name="name" id="wristband_name" placeholder="Enter the name of this wristband" value="<?php echo $row['name']; ?>" />
						<p class="wb_description">Enter the desired name of this set of wristbands. You will use this later to find your wristbands in your database.</p>
					</div>
					<h4>Color</h4>
					<div class="form-field">
						<select name="color" style="width:200px;">
							<option value="#000000" <?php if($row['color']=="#000000") echo "selected"; ?>>Black</option>
							<option value="#ff0000" <?php if($row['color']=="#ff0000") echo "selected"; ?>>Red</option>
							<option value="#ffa500" <?php if($row['color']=="#ffa500") echo "selected"; ?>>Orange</option>
							<option value="#ffff00" <?php if($row['color']=="#ffff00") echo "selected"; ?>>Yellow</option>
							<option value="#008000" <?php if($row['color']=="#008000") echo "selected"; ?>>Green</option>
							<option value="#0000ff" <?php if($row['color']=="#0000ff") echo "selected"; ?>>Blue</option>
							<option value="#cc00cc" <?php if($row['color']=="#cc00cc") echo "selected"; ?>>Violet</option>
						</select>
						<p class="wb_description">Select the desired color for this set of wristbands.</p>
					</div>
					<?php if( ! $this->id ) : ?>
						<h4>Number of sets</h4>
						<div class="form-field">
							<select name="sets">
							<?php for($i=1; $i<11; $i++) : ?>
								<option value="<?php echo $i; ?>"<?php if($row['sets']==$i) echo " selected"; ?>><?php echo $i; ?> Set<?php if($i>1) echo "s"; ?> </option>
							<?php endfor; ?>
							</select>
							<p class="wb_description">Each set shuffles the signals differently using the same ratio of signs.  The same # of each sign will be on different sets, but you can switch them out throughout the season.</p>
							<p class="wb_description wb_urgent">Note: If you have 12 players you do NOT need 12 different sets.  You will need to make 1 set and print 12 copies.</p>
						</div>
					<?php endif; ?>
					<h4>Signs</h4>
					<div class="form-field">
					<?php for($i=0; $i<count($names); $i++) : ?>
					<?php
						if(!$this->id || !$fba[$i]) {
							$fba = array();
							$fba[$i] = 100;
						} 

						$percent = sprintf("%0.2f", $fba[$i]/$total*100);
						$color="grey";

						if($percent<5) {
							$color = "red";
						}
					?>
						<div class="data">
							<div class="data-left">
								<table id="wb_generate_sets">
									<tr>
										<td><input type="text" name="names[]" value="<?php echo $names[$i]; ?>"  maxlength="5" /></td>
										<td><input type="text" name="names2[]" value="<?php echo $names2[$i]; ?>"  maxlength="50" /></td>
										<td>
											<span class="mobile-only">Add/Remove Signs: </span>
											<a id="wb_add" href="#"><img src="<?php echo plugins_url('/images/add.png', __FILE__); ?>" alt="" /></a>
											<a id="wb_remove" href="#" class="remove" style="<?php if(count($names)<=1) echo "display:none"; ?>"><img src="<?php echo plugins_url('/images/delete.png', __FILE__); ?>" alt="" /></a>
										</td>
									</tr>
								</table>
							</div>
							<div class="data-number">(<span class="<?php echo $color; ?>"><?php echo $percent; ?>%</span> of Signs)</div>	
							<div class="clear"></div>
							<div id="fba" class="slider"></div>
							<input type="hidden" name="fba[]" value="<?php echo $fba[$i]; ?>" /> 
						</div>
						<?php endfor; ?>
						<p class="wb_description desktop-only">In the box to the left, enter an acronym for each sign. In the box to the right, enter the full name of the sign. Move the slider left or right to decrease and increase its frequency on the card. Finally, use the "+" and "-" toggles to add or remove signs.</p>
						<p class="wb_description mobile-only">In the top box, enter an acronym for each sign. In the bottom box, enter the full name of the sign. Move the slider left or right to decrease and increase its frequency on the card. Finally, use the "+" and "-" toggles to add or remove signs.</p>
					</div>
					<input type="submit" name="submit" class="button button-primary" />
				</form>
			</div>
		<?php 
			$result = ob_get_contents(); ob_end_clean();
			
			return $result;
		}
		private function process_manage() {
			if ( $this->delete ) {
				$this->manage_delete();
			} elseif ( $_POST['view'] ) {
				$this->manage_download();
			} elseif( $_POST['view_coach'] ) {
				$this->manage_download_coach();
			}
		}
		private function manage_list() {
			global $wpdb;
			
			$query = $wpdb->get_results("SELECT * FROM {$this->objects_table} WHERE user='" . $this->current_user->ID . "' and active='1' ORDER BY ID ASC", ARRAY_A); 
			
			if ( empty( $query ) ) {
				return sprintf( '<p class="error">No wristbands yet. <a href="%s">Click here to create one.</a></p>', $this->generate_url );
			}
			ob_start(); ?>
			<div id="wb_manage_list">
			<h4>Your Generated Wristbands</h4>
				<table id="wb_table_list">
					<tr>
						<th>Name</th>
						<th>Date</th>
						<th>Actions</th>
					</tr>
					<?php $count = 0; ?>
					<?php foreach ($query as $row) : ?>
						<?php ( $count % 2 == 0) ? $class = 'class="even"' : $class = 'class="odd"'; ?>
						<tr <?php echo $class; ?>>
							<td style="vertical-align:middle;"> <?php echo $row['name']; ?> </td>
							<td style="vertical-align:middle;"><?php  echo strftime("%m/%d/%Y", strtotime($row['date'])); ?> </td>
							<td style="width:350px; text-align: center;">
								<a class="button button-primary" href="<?php echo wp_nonce_url($this->manage_url . '?id=' . $row['ID'] . '&delete=1', $this->nonce_action, $this->nonce_name); ?>">Delete</a>
								<a class="button button-primary" href="<?php echo $this->generate_url; ?>?id=<?php echo $row['ID']; ?>">Edit</a>
								<a class="button button-primary" href="<?php echo $this->manage_url; ?>?id=<?php echo $row['ID']; ?>&open=1">Open</a>
							</td>
						</tr>
						<?php $count++; ?>
					<?php endforeach; ?>
				</table>
			</div>
		<?php 
			$result = ob_get_contents(); ob_end_clean();
			
			return $result;
		}
		private function manage_open() {
			global $wpdb;
			
			$row =  $wpdb->get_row("SELECT * FROM {$this->objects_table} WHERE user='" . $this->current_user->ID . "' AND ID='" . $this->id . "'", ARRAY_A);

			if( !$row ) {
				return '<div class="error">Sorry, that wristband doesn\'t exist.</div>';
			} 
			
			ob_start(); ?>
				<div id="wb_manage_open">
				<h4>Download/Print Wristband: <?php echo $row['name']; ?></h4>
					<form class="wb-download-form" action="" method="POST">
						<?php wp_nonce_field( $this->nonce_action, $this->nonce_name ); ?>
						<div class="form-field">
							<table id="wb_table_open">
								<tr>
									<td>Height (in inches)</td>
									<td>Width (in inches)</td>
								</tr>
								<tr>
									<td><input class="wb-input-height" type="text" name="y" value="3" /></td>
									<td><input class="wb-input-width" type="text" name="x" value="5" /></td>
								</tr>
								<tr>
									<td colspan="2">
										<input type="submit" class="button button-primary" name="view" value="Download Player" />
										<input type="submit" class="button button-primary" name="view_coach" value="Download Coach" />
										<span class="wb-alert"><?php echo $error_message; ?></span>
									</td>
								</tr>
							</table>
						</div>
					</form>
				</div>
		<?php
			$result = ob_get_contents(); ob_end_clean();
			
			return $result;
		}
		private function manage_delete() {
			if ( !wp_verify_nonce( $_GET[$this->nonce_name], $this->nonce_action ) ) {
				wp_die("Cheatin' uh?");
			}
			
			global $wpdb;
			
			$wpdb->update( 
				$this->objects_table, 
				array( 
					'active' => 0
				), 
				array( 
					'ID' => $this->id, 
					'user' => $this->current_user->ID 
				), 
				array( 
					'%d'
				), 
				array( '%d', '%d' ) 
			);
							
			wp_redirect($this->manage_url);
			exit();
		}
		private function manage_download() {
			if ( ! wp_verify_nonce( $_POST[$this->nonce_name], $this->nonce_action ) ) {
				wp_die("Cheatin' uh?");
			}
			
			global $wpdb;
			
			$row = $wpdb->get_row("SELECT * FROM {$this->objects_table} WHERE ID='" . $this->id . "' AND user='" . $this->current_user->ID . "'", ARRAY_A);
			
			if ( ! $row ) {
				wp_die("There was a problem creating the PDF for this wristband.");
			}
			
			require_once( dirname(__FILE__) . "/extlib/dompdf-0.6.2/dompdf_config.inc.php");

			$old_limit = ini_set("memory_limit", "512M");
			$times2 = json_decode($row['data']);
			$sign_codes = json_decode($row['data3']);			
			$counter=0;
			$name = $row['name'];
			$file_name = 'NMAS_' . str_replace(' ', '_', $name) . '_player.pdf';

			if( $_POST['x']>8.5 ) 
				$_POST['x']=8.5; // 8.4	
				
			$x = (float) $_POST['x'];
			$y = (float) $_POST['y'];
			$row_color = sanitize_text_field($row['color']);

			// Setup
			$accepted = array(
			    "#000000",
			    "#ff0000",
			    "#ffa500",
			    "#ffff00",
			    "#008000",
			    "#0000ff",
			    "#cc00cc"
			);

			// Checks
			if ( !in_array( $row_color, $accepted, true ) ) {
				$row_color = '#000000';
			}

			$font_color = "#ffffff";
			if ( $row_color == "#ffff00" ){
				$font_color = "#000000";
			}

			// Helpers
			$border = 2;
			$dpi = 72;
			$rows = 13;
			$cols = 18;
			$font_minimum = 4.25;
			$font_maximum = 7.5;
			$numbers1 = array("", "01", "02", "03", "04", "05", "", "11", "12", "13", "14", "15", "", "21", "22", "23", "24", "25");
			$numbers2 = array("", "31", "32", "33", "34", "35", "", "41", "42", "43", "44", "45", "", "51", "52", "53", "54", "55");

			// Variables
			$body_height = $y*$dpi;
			$body_weight = $x*$dpi;
			$height = ($y*$dpi)-5;
			$width = ($x*$dpi)+20;
			$td_height = round(($height/$rows),2)-0.75;
			$td_width = round(($width/$cols),2);

			$calc_font_size = ceil($td_width/2);
			$font_size = ceil($td_width/2)*0.75;

			$code_array = array_map('strlen', $sign_codes);
			$maxlen = max($code_array);
			$minlen = min($code_array);
			$avglen = array_sum($code_array)/count($code_array);
			$totallen = $maxlen + $minlen;

			if( $maxlen === 5 ){
				$calc_font_size = ceil($td_width/3);
				$font_size = ceil($td_width/3)*0.7;
				if ( $x <= 3 ){
					$font_size = ceil($td_width/2)*0.55;
				}
			}
			if( $maxlen === 4 ){
				$calc_font_size = ceil($td_width/2);
				$font_size = ceil($td_width/2)*0.75;
				if ( $x <= 3 ){
					$font_size = ceil($td_width/2)*0.65;
				}
			}

			if( $font_size < $font_minimum ){
				$font_size = $font_minimum;
			} else if ( $font_size > $font_maximum ){
				$font_size = $font_maximum;
			}
			
			$branding_font_size = $font_size;

			ob_start();
			?>
			<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
			<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
				<head>
					<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
					<title></title>
					<?php require_once(dirname(__FILE__) . '/templates/template-player.php'); ?>
				</head>
				<body>
					<table id="table">
						<tr>
						<?php foreach($numbers1 as $val) : ?>			
							<th><?php echo $val; ?></th>
						<?php endforeach; ?>
						</tr>
						<?php for($c=0; $c<5; $c++) : ?>
							<tr>
							<?php for($i=0; $i<3; $i++) : ?> 
								<?php for($a=0; $a<6; $a++) : ?>
									<?php if( $a==0 ) : ?>			
										<th><?php echo ($c+1); ?></th>
									<?php else : ?>
										<td><?php 
										$this_code = ( strlen( trim($times2[$counter]) ) ) ? trim($times2[$counter]) : trim($times2[0]);
										echo '<span style="font-size:'.$font_size.'pt">'.$this_code.'</span>'; $counter++;
										?></td>
									<?php endif; ?>
								<?php endfor; ?>
							<?php endfor; ?>
							</tr>
						<?php endfor; ?>
						<tr>
							<th colspan="18" class="narrow" style="font-size:<?php echo $branding_font_size; ?>pt"><?php echo trim($name); ?> - NeverMissASign.com</th>
						</tr>
						<tr>
						<?php foreach($numbers2 as $val) : ?>			
							<th><?php echo $val; ?></th>
						<?php endforeach; ?>
						</tr>
						<?php for($c=0; $c<5; $c++) : ?>
							<tr>
							<?php for($i=0; $i<3; $i++) : ?>
								<?php for($a=0; $a<6; $a++) : ?>
									<?php if($a==0) : ?>			
										<th><?php echo ($c+1); ?></th>
									<?php else : ?>
										<td><?php 
										$this_code = ( strlen( trim($times2[$counter]) ) ) ? trim($times2[$counter]) : trim($times2[0]);
										echo '<span style="font-size:'.$font_size.'pt">'.$this_code.'</span>'; $counter++;
										?></td>
									<?php endif; ?>
								<?php endfor; ?>
							<?php endfor; ?>

							</tr>
						<?php endfor; ?>	
					</table>
					<table id="legend">
						<tr>
							<td style="text-align:center; font-size:12px; padding-bottom:5px;">
								<?php
								$names2 = json_decode($row['data2'], TRUE);
								$names = json_decode($row['data3'], TRUE);

								for($i=0; $i<count($names); $i++) {
									printf('<p>%s=%s</p>', $names[$i], $names2[$i]);
									$array[] = $names[$i];
								} ?>
							</td>
						</tr>
					</table>
					<p style="text-align:center; font-size:4px; padding-bottom:5px;"><?php  echo 'f: '.$font_size.' s: '.$y.'x'.$x; ?></p>
				</body>
			</html>
			<?php
			$html = ob_get_contents();
			ob_end_clean();
			
			$dompdf = new DOMPDF();
			$dompdf->load_html($html);
			$html = null;
			$dompdf->render();
			$dompdf->stream($file_name);

			exit();	
		}
		private function manage_download_coach() {
			if ( ! wp_verify_nonce( $_POST[$this->nonce_name], $this->nonce_action ) ) {
				wp_die("Cheatin' uh?");
			}
			
			global $wpdb;
			
			$row = $wpdb->get_row("SELECT * FROM {$this->objects_table} WHERE ID='" . $this->id . "' AND user='" . $this->current_user->ID . "'", ARRAY_A);
			$card_name = $row['name'];
			$times2 = json_decode($row['data']);
			$counter=0;
			$row_color = sanitize_text_field($row['color']);
			$file_name = 'NMAS_' . str_replace(' ', '_', $row['name']) . '_coach.pdf';

			for($y=0; $y<2; $y++) {
				if($y==1) $add=30;
				
				for($c=0; $c<5; $c++) { 
					$x = $c+1;
					for($i=0; $i<3; $i++) { 
						for($a=0; $a<5; $a++) { 
							$number = ($i)*(10)+$a+1+$add;
							$number2 = $c+1;
							$number = sprintf("%02d", $number);		
							$list[$number . $number2] = $times2[$counter];
							$counter++;
						} 
					}
				}
			}

			require_once( dirname(__FILE__) . "/extlib/dompdf-0.6.2/dompdf_config.inc.php");
			$old_limit = ini_set("memory_limit", "512M");
			
			ob_start(); ?><!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
			<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
			<title></title>
				<?php require_once(dirname(__FILE__) . '/templates/template-coach.php'); ?>
			</head>
			<body>	
				<table id="legend">
					<?php
						$names2 = json_decode($row['data2'], TRUE); 
						$names = json_decode($row['data3'], TRUE);
						$cellcount = count($names) + (3-count($names)%3);
						
						echo '<tr><td>&nbsp;</td>';
						echo '<td width="200" style="text-align:center;padding-bottom:10px;line-height: auto;"><img style="width: 200px; max-width: 200px;" src="'. dirname(__FILE__) . '/images/nmas_logo2.png" /></td>';
						echo '<td>&nbsp;</td></tr>';

						for($i=1; $i<=$cellcount; $i++) {
							$row = $i % 3;
							if ( $i == 1 ) {
								echo '<tr>';
							} 

							if ( isset($names[$i-1]) ){
								echo '<td>' . $names[$i-1] . " = " . $names2[$i-1] . '</td>';
								$array[] = $names[$i-1];
							} else {
								echo '<td>&nbsp;</td>';
							}

							if ( $i == $cellcount ){
								echo '</tr>';
							} elseif ( $row == 0 && $i<$cellcount) {
								echo '</tr><tr>';
							}
						}
						
					?>
				</table>
				<?php
				$bigarray = $array;
				unset($array);
				$iteration = 0;
				$maxcolumns = 10;	
			
				$lasttable = false;

				for($x=1; $x<=count($bigarray); $x+=$maxcolumns) {	
				$iteration++;
				$array = array();
				$count=0;
				$colcount = $maxcolumns*$iteration;

				if( (count($bigarray) - $maxcolumns) <= 0 ){
					$maxcolumns = count($bigarray);
					$colcount = $maxcolumns*$iteration;

				} elseif( (count($bigarray) - $colcount) <= 0 ){
					$maxcolumns = count($bigarray) - $colcount + $maxcolumns;
					$colcount = $x+$maxcolumns-1;
					$lasttable = true;
				}

				for($i=$x; $i<=$colcount; $i++) {
					if($count==$colcount) {
						break;
					}
					$array[] = $bigarray[$i-1];	
					$count++;
				}
				?>		
				<table class="signs">
					<thead>
					<?php if ( $x == 1 ) { ?>
						<tr>
							<th colspan="<?php echo $maxcolumns; ?>" style="border: 1px solid #000;"><?php echo $card_name; ?></th>
						</tr>
					<?php } ?>
					<tr>
						<?php
						foreach($array as $val) { ?>
						<th style="border: 1px solid #000;white-space: nowrap;"><?php echo trim($val); ?></th>
						<?php } ?>
					</tr>
					</thead>
					<tr>
						<?php
						foreach($array as $val) { 			
						?>
						<td style="border: 1px solid #000;">
							<?php
							$values = array();

							foreach($list as $key=>$val2) { 
								if(strtolower($val2)==strtolower($val)) {					
									$values[] = sprintf("%03d", $key);									
								}
							} 

							sort($values);
							
							foreach($values as $val) {
								 echo $val . " ";
							}
							?>
						</td>
						<?php } ?>
					</tr>
				</table>			
				<?php } ?>
				<div style="position: absolute;bottom: 10px; text-align:center;width: 99%">NeverMissASign.com</div>
			</body></html><?php
			$html = ob_get_contents();
			ob_end_clean();


			$dompdf = new DOMPDF();
			$dompdf->load_html($html);
			$html = null;
			$bigarray = null;
			$dompdf->render();
			$dompdf->stream($file_name);	
			exit();		
		}
	}
}

$wristbander = new Wristbander;