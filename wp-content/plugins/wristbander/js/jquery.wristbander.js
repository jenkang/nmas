jQuery(document).ready(function($){
	var wb_generate = $("#wb_generate"),
		wb_manage_list = $("#wb_manage_list"),
		wb_manage_open = $("#wb_manage_open");

	if ( wb_generate.length || wb_manage_list.length || wb_manage_open.length ){

		var width_floor = 2.75,
			width_ceiling = 6,
			height_floor = 1.75,
			height_ceiling = 4;

		$("input.wb-input-width").on("blur change keyup", function(e){
			var $this   = $(this),
				width   = parseFloat($this.val()),
				height  = parseFloat($("input.wb-input-height").val()),
				errorMessage = validateWidthHeight(width,height);

			$(".wb-alert").html("");
			if( errorMessage.length ){
				$(".wb-alert").html(errorMessage);
			}
		});

		$("input.wb-input-height").on("blur change keyup", function(e){
			var $this  = $(this),
				width  = parseFloat($("input.wb-input-width").val()),
				height = parseFloat($this.val()),
				errorMessage = validateWidthHeight(width,height),
				number_per_page = 3;

			$(".wb-alert").html("");
			if( errorMessage.length ){
				$(".wb-alert").html(errorMessage);
			}
		});

		$("input.wb-submit-view").on("click", function(e){
			var $this  = $(this),
				width  = parseFloat($("input.wb-input-width").val()),
				height = parseFloat($("input.wb-input-height").val()),
				errorMessage = validateWidthHeight(width,height);

			$(".wb-alert").html("");
			if( errorMessage.length ){
				$(".wb-alert").html(errorMessage);
				return false;
			} else {
				return true
			}
		});

		$("form.wb-download-form").on("submit", function(e){
			var $this  = $(this),
				width  = parseFloat($("input.wb-input-width").val()),
				height = parseFloat($("input.wb-input-height").val()),
				errorMessage = validateWidthHeight(width,height);

			$(".wb-alert").html("");
			if( errorMessage.length ){
				$(".wb-alert").html(errorMessage);
				return false;
			} else {
				return true
			}
		});

		function validateWidthHeight(w,h){
			var errorMessage = "";

			if( isNaN(w) ){
				errorMessage = "Width is not a number.";
			}

			if( isNaN(h) ){
				if( errorMessage.length ){
					errorMessage = errorMessage + "<br>";
				}
				errorMessage = errorMessage + "Height is not a number.";
			}

			if( w < width_floor ){
				if( errorMessage.length ){
					errorMessage = errorMessage + "<br>";
				}
				errorMessage = errorMessage + "Width is too small to print legibly.";
			}

			if( w > width_ceiling ){
				if( errorMessage.length ){
					errorMessage = errorMessage + "<br>";
				}
				errorMessage = errorMessage + "Width is too large. Width limit is " + width_ceiling + " inches.";
			}

			if( h < height_floor ){
				if( errorMessage.length ){
					errorMessage = errorMessage + "<br>";
				}
				errorMessage = errorMessage + "Height is too small to print legibly.";
			}

			if( h > height_ceiling ){
				if( errorMessage.length ){
					errorMessage = errorMessage + "<br>";
				}
				errorMessage = errorMessage + "Height is too large. Height limit is " + height_ceiling + " inches.";
			}

			if( w < h ){
				if( errorMessage.length ){
					errorMessage = errorMessage + "<br>";
				}
				errorMessage = errorMessage + "Width must be greater than height to print legibly.";
			}

			return errorMessage;
		}
	
		function remove() {
			var $this = $(this),
				size = $(".data").size(),
				target = $this.parents('.data').first();
			
			target.remove();	

			if(size==1) {
				$(".remove").hide();
			}

			update_percentages();
			
			return false;
		}
		
		var id=1;
		var num=1;
		var div_count=0;

		function add() {
			var html = "";
			html = $(".data").first().clone().html();	
			$(".data").last().after("<div class='data' id='data_" + num + "'>"+html+"</div>");	

			$("#data_"+num+" .slider").slider({

				change: function(event, ui) {
					$(this).next().val(ui.value);
					update_percentages();
				}, slide: function(event, ui) {
					$(this).next().val(ui.value);
					update_percentages();
				},		
				min: 10, max: 100, value: 100, step: 1

			});

			$(".remove").show();
			update_percentages();

			num++;

			id++;
			
			return false;
		}
		
		$(".slider").each(
		function() {

			var val = $(this).next().val();
			$(this).slider(

			{	change: function(event, ui) {
					$(this).next().val(ui.value);
					update_percentages();
				}, slide: function(event, ui) {
					$(this).next().val(ui.value);
					update_percentages();
				},
				min: 1, max: 100, value: val, step: 1
			});

		});

		function update_percentages() {

			var total=0;
			$("input[name^=fba]").each(function() {
				total += parseInt($(this).val());
			});
			$("input[name^=fba]").each(function() {
				var $this = $(this);
				var percent = ($this.val()/total*100),
					percent = percent.toFixed(2),
					number = 150 * (percent/100),
					number = number.toFixed(0);
				var color = "grey";
				if(percent<5) {
					var color="red";
				}
				$(this).parent().find(".data-number").html("(<span class='" + color + "'>" + percent + "%</span> of signs or about " + number + " times)");
			});

		}
			
		$('body').on('click', '#wb_add', add);
		$('body').on('click', '#wb_remove', remove);
	}
});