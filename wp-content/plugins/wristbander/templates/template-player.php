<style>
body {
	width: 100%;
	margin:0;
	padding:0;
	font-family: Helvetica;
	font-size: 100%;
}

#table {
	width: <?php echo $width; ?>px;
	height: <?php echo $height; ?>px;
	margin: 20px auto 0 auto;
	border-spacing: 0;
	border-collapse: collapse;
	border: 1px solid #000;
	table-layout: fixed;
}

#table td, #table th {
	width: <?php echo $td_width; ?>px;
	height: <?php echo $td_height; ?>px;
	text-align: center;
	border: 1px solid #000;
	border-spacing: 0;
	border-collapse: collapse;
	font-size: <?php echo $font_size; ?>pt;
	vertical-align: middle;
	overflow: hidden;
}

#table td {
	font-size: <?php echo $font_size; ?>pt;
	white-space: nowrap;
	overflow: hidden;
}

#table th {
	color:<?php echo $font_color; ?>;
	background-color: <?php echo $row_color; ?>;
	text-transform: uppercase;
}

#legend {
	width: 200px;
	margin: 20px auto;
	border: 1px solid #000;
	padding: 10px;
}

#legend td p {
	padding: 0;
	margin: 0;
}
</style>