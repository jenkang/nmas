<?php
// Set to CSS content type
header("Content-type: text/css; charset: UTF-8");

// Setup
$accepted = array(
			    "#000000",
			    "#ff0000",
			    "#ffa500",
			    "#ffff00",
			    "#008000",
			    "#0000ff",
			    "#cc00cc"
			);

// Checks
if ( !in_array( $row_color, $accepted ) ) {
	$row_color = '#000000';
}

$font_color = "#ffffff";
if ( $row_color == "#ffff00" ){
	$font_color = "#000000";
}
?>
<style>
body, html {
	font-family: Helvetica;
	font-size: 100%;
	padding: 0;
	margin: 0;
}

table {
	width: 96%;
	margin: 20px auto 0 auto;
	border: 1px solid #000;
	border-spacing: 0;
}

th, td {
	border: 1px solid #000;
	border-spacing: 0;
	text-align: center;
	text-transform: uppercase;
}

th {
	color:<?php echo $font_color; ?>;
	background-color: #f1f1f1;
	background-color: <?php echo $row_color; ?>;
}

td {
	line-height: 1.5rem;
	padding:10px 0px 10px 2px;
	font-size:11px;
}

#legend {
	border: none;
}

#legend th,
#legend td {
	border: none;
	text-align: left;
	padding: 0;
}
</style>