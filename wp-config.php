<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nmasdbase');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('DISALLOW_FILE_EDIT', TRUE); // Sucuri Security: Wed, 10 Feb 2016 18:44:49 +0000


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'X^7-<n30ljGzTJ1~2)d?[~1<^]xGx?PqNxyFj:#dLD46iGg=px}emkAKk;|Q=i|-');
define('SECURE_AUTH_KEY',  '-*-?[zsY}VM_--.+9r|ti]0TJROLlzHk[N^3wrVNbfK!3U#2;{xJ|2~|sSpmdAZ ');
define('LOGGED_IN_KEY',    'm[NOC9bg[[5#,~I9A~R]b|X19[+PLax|COp,&N,6L|Up_B?L_+2jVC8iF+T}de[a');
define('NONCE_KEY',        '}3|NYJ1lhAVtJf>r|@u_kMD7}ACs4]q8!IAQwT)PV?,-M1j@Bt 8h*@h~()a@l)!');
define('AUTH_SALT',        '?c-Y-J=.1HIW9}w-F-1-.u(`U&Yk}_5-<KQa!%sGR1>+7k3_HAiy?PN`P9vjCyr|');
define('SECURE_AUTH_SALT', 'HbWwmOhQA:Ee|,w4$+UB9bN+xA?;a>QBq:=b1q|u@0|r>rq| I_vz [l1Dk4V*;[');
define('LOGGED_IN_SALT',   'DJ9R{6bhvtH3xVL75+nsq,[2xs-~4NUm}f2V4!e6o3XmQ.sTXv&uqJTV+GlF@LzL');
define('NONCE_SALT',       '6AUk1|uO/Rp;u_@zP.Cbh4Ag&)VZ6=/_)k?~u-aDYkO7xc!_v,|?-1?G*8:!qyQG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'nmaswp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
